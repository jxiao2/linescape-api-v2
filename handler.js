'use strict';

//import handlers from "/lib/handlers/dataApi.js";

module.exports.helloSlsV2 = async event => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'This is linescape-api-v2-try, built on Serverless 2, for doing pre-migration testing. ',
        input: event,
      },
      null,
      2
    ),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
