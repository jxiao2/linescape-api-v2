'use strict';

module.exports = {
  searchLimitExceeded: "SearchLimitExceeded",
  validationError: "ValidationError",
  unknownError: "ServerError",
  timeoutError: "TimeoutError"
};