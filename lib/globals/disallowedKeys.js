"use strict";

import accountTypes from "./accountTypes.js";
import _ from "lodash";

var DISALLOWED_KEYS = {
  vessel: {},
  carrier: {},
  schedule: {}
};

// Vessels

DISALLOWED_KEYS.vessel[accountTypes.premium] = [
  "ship_type",
  "port_registry",
  "year_built",
  "ship_builder",
  "yard_hull",
  "handling_gear",
  "length",
  "beam",
  "draught",
  "engine_design",
  "engine_type",
  "power",
  "speed",
  "sfc",
  "co2"
];

DISALLOWED_KEYS.vessel[accountTypes.basic] = _.union(DISALLOWED_KEYS.vessel[accountTypes.premium], [
  "owner",
  "carrier",
  "previous_names",
  "gross_tonnage",
  "net_tonnage",
  "dwt",
  "avg_teu",
  "max_teu",
  "reefer_teu"
]);

DISALLOWED_KEYS.vessel[accountTypes.unregistered] = _.union(DISALLOWED_KEYS.vessel[accountTypes.basic], [
]);

// Carriers

DISALLOWED_KEYS.carrier[accountTypes.premium] =  [

];


DISALLOWED_KEYS.carrier[accountTypes.basic] = _.union(DISALLOWED_KEYS.carrier[accountTypes.premium], [

]);

DISALLOWED_KEYS.carrier[accountTypes.unregistered] = _.union(DISALLOWED_KEYS.carrier[accountTypes.basic], [

]);


// Schedules

DISALLOWED_KEYS.schedule[accountTypes.premium] = [

];

DISALLOWED_KEYS.schedule[accountTypes.basic] = _.union(DISALLOWED_KEYS.schedule[accountTypes.premium], [
  "last_update"
]);

DISALLOWED_KEYS.schedule[accountTypes.unregistered] = _.union(DISALLOWED_KEYS.schedule[accountTypes.basic], [
  "route"
]);

module.exports = DISALLOWED_KEYS;