'use strict';

module.exports = {
  unregistered: "Unregistered",
  basic: "Basic",
  premium: "Premium",
  professional: "Professional"
};