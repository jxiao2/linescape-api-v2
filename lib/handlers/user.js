"use strict";

import AWS from "aws-sdk";
import https from "https";
import dynamo from "../dynamo";
import api from "../lshift-api";
import billing from "./billing.js";
import Promise from 'bluebird';
import utils from "../utils";
import Error from "../models/error.js";
import momentTimezone from "moment-timezone";
import _ from 'lodash';
import errorCodes from "../globals/errorCodes.js";
import DeliverySettingsValidator from "../validators/deliverySettingsValidator.js";
import EmailScheduleValidator from "../validators/emailScheduleValidator.js";
import RegisterNewUserValidator from "../validators/registerNewUserValidator.js";
import ContactEmailValidator from '../validators/contactEmailValidator.js';
import smtp from '../utils/smtp.js';

import MailChimp from "mailchimp-lite";
import crypto from "crypto";

module.exports.registerNewUser = (event, context, callback) => {
  console.log('register new user', event.data);
  var cognitoId = utils.getCognitoId(event);
  var data = event.data;
  var validator = RegisterNewUserValidator;
  var errors = validator.validate(data);
  var error;

  if (errors.length > 0) {
    error = new Error(400,
      "A validation error occurred. See the details for more information.",
      errorCodes.validationError,
      errors);
    callback(JSON.stringify(error));
    return;
  }
  // if ((data.coupon === undefined && data.token === undefined) || data.planId === undefined) {
  //   error = new Error(400, null, null, "call to register a customer with a token or coupon and planId");
  //   callback(JSON.stringify(error));

  //   dynamo.makeUserRegistered
  // }
  // else if ((data.coupon !== undefined || data.token !== undefined) && data.planId !== undefined) {
    console.log('b');
    dynamo.makeCustomerRegistered(cognitoId, data.email, data.token, data.coupon, data.planId, data.firstName, data.lastName).then(response => {
      callback(null, null);
    }).catch(err => {
      error = new Error(400, null, null, err);
      callback(JSON.stringify(error));
    });
  // }
  // else {
  //   console.log('c');
  //   error = new Error(400, null, null, "token or coupon and planId must be set for customer registrations");
  //   callback(JSON.stringify(error));
  // }
};

module.exports.contact = (event, context, callback) => {
  let data = event.data;
  let validator = ContactEmailValidator;
  let errors = validator.validate(data);
  let error = '';
  if (errors.length > 0) {
    error = new Error(400,
      "A validation error occurred. See the details for more information.",
      errorCodes.validationError,
      errors);
    callback(JSON.stringify(error));
    return;
  }

  let htmlObj = {};
  htmlObj.contact = data;

  let html = smtp.createHTMLResultsForEmail(htmlObj);

  let subject = "Linescape Contact Form";
  try {
    subject = `Linescape ${data.type} - ${data.company}, ${data.country}`;
  } catch (ex) {
    console.log('email subject creation failed', data, ex);
    subject = "Linescape Contact Form";
  }

  let from = data.email;
  try {
    from = `${data.from} <${data.email}>`;
  } catch (ex) {
    console.log('email from creation failed', data, ex);
    from = data.email; 
  }
  // smtp.send(from, "contacts@linescape.com", subject, "A new contact form filled out for you to peruse", html, null);
  // smtp.send(from, "linescape@pipedrivemail.com", subject, "A new contact form filled out for you to peruse", html, null);
  // smtp.send(from, "mdurkin@tecture.com", subject, "A new contact form filled out for you to peruse", html, null);
  
  //working version below
  // smtp.send("contacts@linescape.com", from, subject, "A new contact form filled out for you to peruse", "Thank you for submitting the following request to Linescape. One of our representatives will respond as soon as possible", null )

  smtp.send("contacts@linescape.com", from, subject, "Thank you for submitting the following request to Linescape. One of our representatives will respond as soon as possible.", html, null )
  smtp.send(from, "contacts@linescape.com", subject, "Thank you for submitting the following request to Linescape. One of our representatives will respond as soon as possible.", html, null )
};

module.exports.addPersonalInformation = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  var data = event.data;
  
  dynamo.getUserProfile(cognitoId).then(profile => {
    console.log('setting personal information with data', data);

    var personalInformation = {
      firstName: data.firstName,
      lastName: data.lastName,
      business: data.business,
      company: data.company,
      position: data.position,
      country: data.country,
      city: data.city
    };

    var mergeFields = {
      FNAME: data.firstName,
      LNAME: data.lastName,
      MMERGE3: profile.registerDate,
      MMERGE5: data.company,
      MMERGE6: data.position,
      MMERGE8: data.city,
      MMERGE9: data.country,
      MMERGE12: data.business
    };

    console.log('before initializing the mailchimp api');
    var mailchimp = new MailChimp({
      key: process.env.MAILCHIMP_KEY,
      datacenter: "us15"
    });
    var emailHash = crypto.createHash('md5').update(profile.email).digest('hex');
    console.log('after initializing the mailchimp api');

    mailchimp.get(`/lists/${process.env.MAILCHIMP_LIST_ID}/members/${emailHash}`)
      .then((mailchimpUser) => {
        console.log('email was found in the mailchimp list', mailchimpUser);
        // in the mailchimp list
        // update the data in mailchimp

        var body = {
          status: data.subscribe == 'true' ? "subscribed" : "unsubscribed",
          merge_fields: mergeFields
        };

        mailchimp.put(`/lists/${process.env.MAILCHIMP_LIST_ID}/members/${emailHash}`, body).then((putResponse) => {
          console.log('information was updated in mailchimp', putResponse);
          // save personal information into the database
          dynamo.setUserPersonalInformation(cognitoId, personalInformation).then(personal => {
            console.log('setUserPersonalInformation return', personal);
            callback(null, personal);
          }).catch(err => {
            var error = new Error(400, "Error adding user's personal information.", null, err);
            callback(JSON.stringify(error));
          });
        }).catch((err) => {
          // return error to user
          var error = new Error(400, "Unable to connect to our marketing partner.", null, err);
          callback(JSON.stringify(error));
        });
      }).catch((err) => {
        // not in the list
        // send to mailchimp if they want to subscribe
        if (data.subscribe == 'true') {
          // wants to be added to the mailing list

          var body = {
            email_address: profile.email,
            status: "subscribed",
            merge_fields: mergeFields
          };

          console.log('user is not in mailchimp, but would like to be', `/lists/${process.env.MAILCHIMP_LIST_ID}/members/`, body);

          mailchimp.post(`/lists/${process.env.MAILCHIMP_LIST_ID}/members/`, body).then((postResponse) => {
              console.log('information was added to mailchimp', postResponse);
              // save personal information into the database
              dynamo.setUserPersonalInformation(cognitoId, personalInformation).then(personal => {
                console.log('setUserPersonalInformation return', personal);
                callback(null, personal);
              }).catch(err => {
                var error = new Error(400, "Error adding user's personal information.", null, err);
                callback(JSON.stringify(error));
              });
          }).catch((err) => {
            console.log('error from mailchimp', err);
            // return error to user
            var error = new Error(400, "Unable to connect to our marketing partner.", null, err);
            callback(JSON.stringify(error));
          });
        } else {
          console.log('user does not want to be subscribed to mailchimp');
          dynamo.setUserPersonalInformation(cognitoId, personalInformation).then(personal => {
            console.log('setUserPersonalInformation return', personal);
            callback(null, personal);
          }).catch(err => {
            var error = new Error(400, "Error adding user's personal information.", null, err);
            callback(JSON.stringify(error));
          });
        }
      });
  }).catch(err => {
    var error = new Error(400, "Unable to get user profile.", null, err);
    callback(JSON.stringify(error));
  });
};

module.exports.confirmNewUser = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  var data = event.data;
  var validator = RegisterNewUserValidator;
  var errors = validator.validate(data);
  var error = '';
  if (errors.length > 0) {
    error = new Error(400,
      "A validation error occurred. See the details for more information.",
      errorCodes.validationError,
      errors);
    callback(JSON.stringify(error));
    return;
  }
  // get account
  dynamo.getUserProfile(cognitoId).then(profile => {
    console.log('check', profile);
    console.log("cognitoId", cognitoId);
    if (typeof profile === 'undefined') {
      console.log("profile not found");
      dynamo.getUserFromEmail(data.email).then(user => {
        // Imported user registration
        if (!user) {
          console.log("no user found through email, either! creating new user");
          dynamo.makeUserRegistered(cognitoId, data.email).then(user => {

            callback(null, null);
          }).catch(err => {
            var error = new Error(400, null, null, err);
            callback(JSON.stringify(error));
          });
        }
        else {
          console.log("found user in getUserFromEmail (from getUserProfile) with email: ", data.email);

          module.exports.registerProfile(cognitoId, user).then(result => {
            console.log("registering user from getuserprofile");
            callback(null, result);
          }).catch(err => {
            var error = new Error(400, null, null, err);
            callback(JSON.stringify(error));
          });
        }
      });
    }
    else  {
      module.exports.registerProfile(cognitoId, profile).then(result => {
        callback(null, result);
      }).catch(err => {
        var error = new Error(400, null, null, err);
        callback(JSON.stringify(error));
      });
    }
  }).catch(err => {
    console.log('confirmNewUser in catch(err)');

    dynamo.makeUserRegistered(cognitoId, data.email).then(user => {
      console.log("succeeded in making user registered in catch(err)", cognitoId);
      // create stripe customer and subscribe them to appropriate account
      /*billing.confirmUser(cognitoId, profile).then(result => {
        callback(null, result);
      }).catch(err => {
        var error = new Error(400, null, null, err);
        callback(JSON.stringify(error));
      });*/
      callback(null, null);
    }).catch(err => {
      var error = new Error(400, null, null, err);
      callback(JSON.stringify(error));
    });

  });
};

module.exports.registerProfile = (cognitoId, profile) => {
  console.log('confirmNewUser making customer (about to call makeUserRegistered)');
  // make basic account
  return new Promise((resolve, reject) => {
    dynamo.makeUserRegistered(cognitoId, profile.email).then(user => {
      // create stripe customer and subscribe them to appropriate account
      if ((profile.account.coupon !== undefined || profile.account.token !== undefined) && profile.account.planId !== undefined) {
        billing.confirmUser(cognitoId, user).then(result => {
          console.log("billing confirm user success");
          return resolve(result);
        }).catch(err => {
          reject(err);
        });
      }
      else {
        resolve(user);
      }
    }).catch(err => {
      reject(err);
    });
  });
};

module.exports.confirmAccount = (event, context, callback) => {
  // user is expected to be unauthenticated
  var query = JSON.parse(event.query);

  if (query.email === null) {
    error = new Error(400,
      "A validation error occurred. See the details for more information.",
      errorCodes.validationError,
      "no email set");
    callback(JSON.stringify(error));
    return;
  }

  var email = query.email;

  dynamo.checkIfUser(email).then(result => {
    callback(null, null); // result will be null
  }).catch(err => {
    var error = new Error(400, null, null, err);
    callback(JSON.stringify(error));
  });
};

/**
 * Sets the user's note for a given schedule.
 */
module.exports.addScheduleNote = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  dynamo.addScheduleNote(cognitoId, utils.getParam(event.params, "scheduleId", "int"),
      event.data.note)
    .then(response => {
      callback(null, null);
    }).catch(err => {
      var error = new Error(400, null, null, err);
      callback(JSON.stringify(error));
    });
};

/**
 * Retrieves the user's note for a given schedule..
 */
module.exports.getScheduleNote = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  var scheduleId = utils.getParam(event.params, "scheduleId", "int");
  dynamo.getScheduleNote(cognitoId, scheduleId)
    .then(response => {
      var note = "";
      if (response.Count > 0) {
        note = response.Items[0].note;
      }

      var json = {
        note: note,
        scheduleId: scheduleId
      };

      callback(null, json);
    }).catch(err => {
      var error = new Error(400, null, null, err);
      callback(JSON.stringify(error));
    });
};

/**
 * Retrieves the current user's profile.
 */
module.exports.getCurrentUserProfile = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  dynamo.getUserProfile(cognitoId).then(user => {
    console.log('getCurrentUserProfile before getUserAccountType', user);
    dynamo.getUserAccountType(cognitoId).then(accountType => {
      if (user.limits === undefined) {
        user.limits = {};
      }
      user.limits.maxSchedules = accountType.limits.maxSchedules;
      console.log('getCurrentUserProfile then of getUserAccountType', accountType);
      callback(null, user);
    }).catch(err => {
      callback(null, user);
    });
  }).catch(err => {
    var error = new Error(400, null, null, err);
    callback(JSON.stringify(error));
  });
};

/**
 * Retrieves the user's delivery settings
 */
module.exports.getDeliverySettings = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);

  dynamo.getDeliverySettings(cognitoId).then((response) => {
    callback(null, response);
  }).catch(err => {
    var error = new Error(400,
      "A validation error occurred. See the details for more information.",
      errorCodes.validationError,
      err);
    callback(JSON.stringify(error));
  });
};

/**
 * Updates the user's delivery settings
 */
module.exports.updateDeliverySettings = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  var validator = DeliverySettingsValidator;
  var errors = validator.validate(event.data);

  if (errors.length > 0) {
    var error = new Error(400,
      "A validation error occurred. See the details for more information.",
      errorCodes.validationError,
      errors);
    callback(JSON.stringify(error));
    return;
  }

  dynamo.updateDeliverySettings(cognitoId, event.data).then(() => {
    callback(null, null);
  }).catch(err => {
    var error = new Error(500,
      "An unknown error occurred. See the details for more information.",
      errorCodes.serverError,
      err);
    callback(JSON.stringify(error));
  });
};

/**
 * Retrieves the user's email schedules.
 */
module.exports.getEmailSchedules = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);

  dynamo.getEmailSchedules(cognitoId).then((response) => {
    var portIds = {};
    var originPortIds = {};
    var destinationPortIds = {};
    var vesselIds = {};

    var apiCalls = [];
    var promisePostResponse = [];
    var setDataFunction = (key, index) => {
      return function(result) {
        if (result.items.length > 0) {
          response[index][key] = result.items[0];
        }
        else {
          response[index][key] = null;
        }
      };
    };

    if (response) {
      for (var i = 0; i < response.length; ++i) {
        var item = response[i];
        var index = i;
        if (typeof item.portId !== 'undefined') {
          apiCalls.push(api.ports({
            id: item.portId
          }));
          promisePostResponse.push(setDataFunction("port", i));
        }

        if (typeof item.originPortId !== 'undefined') {
          apiCalls.push(api.ports({
            id: item.originPortId
          }));
          promisePostResponse.push(setDataFunction("originPort", i));
        }

        if (typeof item.destinationPortId !== 'undefined') {
          apiCalls.push(api.ports({
            id: item.destinationPortId
          }));
          promisePostResponse.push(setDataFunction("destinationPort", i));
        }

        if (typeof item.vesselId !== 'undefined') {
          apiCalls.push(api.vessels({
            id: item.vesselId
          }));
          promisePostResponse.push(setDataFunction("vessel", i));
        }
      }

      Promise.all(apiCalls).then(result => {
        for (var i = 0; i < result.length; ++i) {
          promisePostResponse[i](result[i]);
        }
        callback(null, response);
      });
    }
  }).catch(err => {
    var error = new Error(400,
      "A validation error occurred. See the details for more information.",
      errorCodes.validationError,
      err);
    callback(JSON.stringify(error));
  });
};

/**
 * Adds an email schedule if the user isn't at their limit.
 */
module.exports.addEmailSchedule = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  var validator = EmailScheduleValidator;
  var errors = validator.validate(event.data);
  if (errors.length > 0 || event.data.scheduleId === undefined) {
    var error = new Error(400,
      "A validation error occurred. See the details for more information.",
      errorCodes.validationError,
      errors);
    callback(JSON.stringify(error));
    return;
  }

  dynamo.rejectOnEmailScheduleCountExceeded(cognitoId, event.data.scheduleId).then(() => {
    dynamo.addEmailSchedule(cognitoId, event.data).then((result) => {
      callback(null, result);
    }).catch(err => {
      var error = new Error(500,
        "An unknown error occurred. See the details for more information.",
        errorCodes.serverError,
        err);
      callback(JSON.stringify(error));
    });
  }).catch(err => {
    var error = new Error(400,
      "Email schedule limit exceeded",
      errorCodes.validationError,
      err);
    callback(JSON.stringify(error));
  });
};

/**
 * Removes an email schedule.
 */
module.exports.removeEmailSchedule = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  var scheduleId = utils.getParam(event.params, "scheduleId", "string");
  // if (errors.length > 0) {
  //   var error = new Error(400,
  //     "A validation error occurred. See the details for more information.",
  //     errorCodes.validationError,
  //     errors);
  //   callback(JSON.stringify(error));
  //   return;
  // }

  dynamo.removeEmailSchedule(cognitoId, scheduleId).then(() => {
    callback(null, null);
  }).catch(err => {
    var error = new Error(500,
      "An unknown error occurred. See the details for more information.",
      errorCodes.serverError,
      err);
    callback(JSON.stringify(error));
  });
};