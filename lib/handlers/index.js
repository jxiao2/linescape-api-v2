"use strict";

import AWS from "aws-sdk";
import https from "https";
import dynamo from "../dynamo";
import api from "../lshift-api";
import Promise from 'bluebird';
import utils from "../utils";
import Error from "../models/error.js";
import momentTimezone from "moment-timezone";
import _ from 'lodash';
import errorCodes from "../globals/errorCodes.js";

/**
 * Adds a client error to the logs.
 */
module.exports.addErrorLog = (event, context, callback) => {
  dynamo.addErrorLog(event.data).then(response => {
    callback(null, null);
  }).catch(err => {
    var error = new Error(400, null, null, err);
    callback(JSON.stringify(error));
  });
};

/**
 * Retrieves client errors from the logs.
 */
module.exports.getErrorLogs = (event, context, callback) => {

  dynamo.getErrorLogs().then(response => {
    callback(null, response);
  }).catch(err => {
    var error = new Error(400, null, null, err);
    callback(JSON.stringify(error));
  });
};

/**
 * Retrieves all available timezones.
 */
module.exports.timezones = (event, context, callback) => {
  var names = momentTimezone.tz.names();
  _(names).forEach(function(value, index, collection) {
    collection[index] = value.replace(/_/g, ' ');
  });
  callback(null, names);
};