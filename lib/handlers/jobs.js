"use strict";

import dynamo from "../dynamo";
import smtp from "../utils/smtp.js";
import Promise from 'bluebird';
import api from "../lshift-api";
import utils from "../utils";
import moment from "moment";
import _ from "lodash";

module.exports.sendScheduleEmails = (event, context, callback) => {
  dynamo.getDeliverySchedulesForHour().then(result => {
    var schedules = result.Items;
    var calls = [];
    for (var i = 0; i < schedules.length; ++i) {
      calls.push(sendEmailSchedule(schedules[i]));
      calls.push(dynamo.updateLastEmailDate(schedules[i].cognitoId));
    }

    Promise.all(calls).then(() => {
      callback(null, schedules);
    });
  });
};

var sendEmailSchedule = schedule => {
  return new Promise((resolve, reject) => {
    try {
      var otherEmails = schedule.otherEmails || [];
      var calls = getEmailScheduleContent(schedule);
      Promise.all(calls).then(results => {
        if (schedule.emailSelf) {
          dynamo.getUserProfile(schedule.cognitoId).then(result => {
            otherEmails.push(result.email);
            emailResults(results, schedule, otherEmails)
              .then(() => {
                resolve();
              })
              .catch(err => {
                console.log("err in email results", err);
                reject(err);
              });
          });
        }
        else {
          emailResults(results, schedule, otherEmails)
            .then(() => resolve());
        }
      });
    }
    catch (ex) {
      console.log("error in send", ex);
      reject(ex);
    }
  });
};


var emailResults = (results, schedule, emails) => {
  return new Promise((resolve, reject) => {
    var calls = [];
    var columns = [];
    var mappedResults = [];
    console.log('emailResults results', results);
    console.log('emailResults schedule', schedule);
    console.log('emailResults emails', emails);

    for (var i = 0; i < results.length; ++i) {
      var csvMap;

      try {
        if (schedule.emailSchedules[i].type === 'portPair') {
          csvMap = utils.mapCsv(results[i].items, 'trip');
        }
        else if (schedule.emailSchedules[i].type === 'vessel') {
          csvMap = utils.mapCsv(results[i].items, 'vesselschedule');
        }
        else {
          csvMap = utils.mapCsv(results[i].items, 'portschedule');
        }

        var filtered = [];

        for (var j = 0; j < csvMap.length; j++) {
          var copy = JSON.parse(JSON.stringify(csvMap[j]));
          delete copy.carrierId;

          filtered.push(copy);
          //delete filtered[j].carrierId;
        }

        mappedResults.push(csvMap);
        calls.push(utils.createCsv(filtered));
      }
      catch (ex) {
        mappedResults.push([]);
        calls.push(utils.createCsv([]));
      }
    }
    console.log('after mapCsv mappedResults', mappedResults);

    Promise.all(calls).then(csvResults => {
      var attachments = [];
      var html = [];
      var emailSubjects = [];

      console.log('csvResults', csvResults);
      for (var i = 0; i < csvResults.length; ++i) {
        if (csvResults[i] === '') {
          continue;
        }
        console.log('before attachments', csvResults[i]);
        try {
          var fileSuffix = '';
          var emailSubject = "Linescape Shipping Schedules";

          let htmlObj = {};
          htmlObj.total = results[i].summary.count;
          htmlObj.limit = 100 < htmlObj.total ? 100 : htmlObj.total;
          htmlObj.startDate = moment().format("YYYY-MM-DD");
          htmlObj.endDate = moment().add(1, 'month').format("YYYY-MM-DD");

          if (schedule.emailSchedules[i].type === 'port') {
            htmlObj.ports = mappedResults[i];
            try {
              htmlObj.portName = htmlObj.ports[0].portName;
              htmlObj.portId = schedule.emailSchedules[i].portId;

              fileSuffix = htmlObj.ports[0].portCode;
              emailSubject = `Linescape Port Arrivals/Departures at ${fileSuffix}`;
            }
            catch (ex) {
              console.log('port schedule exception', ex);
              htmlObj.portName = '';
              htmlObj.portId = 0;
            }
          }
          else if (schedule.emailSchedules[i].type === 'vessel') {
            htmlObj.vessels = mappedResults[i];

            try {
              htmlObj.vesselName = htmlObj.vessels[0].vesselName;
              htmlObj.vesselIMO = htmlObj.vessels[0].vesselIMO;
              htmlObj.vesselId = schedule.emailSchedules[i].vesselId;

              fileSuffix = htmlObj.vesselIMO;
              emailSubject = `Linescape Vessel Schedule for IMO-${fileSuffix}`;

              var formatted = [];
              // hate having to do this
              for (var j = 0; j < mappedResults[i].length; j++) {
                var item = mappedResults[i][j];
                
                var tracked = -1;
                for (var k = 0; k < formatted.length; k++) {
                  if (formatted[k].voyage == item.voyage && formatted[k].carrierSCAC == item.carrierSCAC && formatted[k].routeCode == item.routeCode) {
                    tracked = k;
                  }
                }

                if (tracked != -1) {
                  formatted[tracked].schedules.push(item);
                }
                else {
                  formatted.push({
                    carrierId: item.carrierId,
                    carrierName: item.carrierName,
                    carrierSCAC: item.carrierSCAC,
                    voyage: item.voyage,
                    routeName: item.routeName,
                    routeCode: item.routeCode,
                    schedules: [item]
                  });
                }
              }
              htmlObj.vessels = formatted;
            }
            catch (ex) {
              console.log('vessel schedule exception', ex);
              htmlObj.vesselName = '';
              htmlObj.vesselIMO = '';
              htmlObj.vesselId = 0;
            }
          }
          else {
            htmlObj.schedules = mappedResults[i];
            try {
              var startPortId = schedule.emailSchedules[i].originPortId;
              var endPortId = schedule.emailSchedules[i].destinationPortId;

              htmlObj.startPortName = htmlObj.schedules[0].startPortName;
              htmlObj.startPortCode = htmlObj.schedules[0].startPortCode;
              htmlObj.endPortName = htmlObj.schedules[0].endPortName;
              htmlObj.endPortCode = htmlObj.schedules[0].endPortCode;
              htmlObj.route = '/schedules;' +
                'origin_search=' + htmlObj.startPortName +
                ';origin_id=' + startPortId +
                ';destination_search=' + htmlObj.endPortName +
                ';destination_id=' + endPortId +
                ';start_date_gte=' + htmlObj.startDate +
                ';start_date_lte=' + htmlObj.endDate;
              console.log('HTML OBJ FOR SCHEDULES', htmlObj);

              fileSuffix = `${htmlObj.startPortCode}_${htmlObj.endPortCode}`;
              emailSubject = `Linescape Port Pair Schedule from ${htmlObj.startPortCode} to ${htmlObj.endPortCode}`;
            }
            catch (ex) {
              console.log('portPair schedule exception', ex);
              htmlObj.startPortName = '';
              htmlObj.startPortCode = '';
              htmlObj.endPortName = '';
              htmlObj.endPortCode = '';
              htmlObj.route = '';
            }
          }
          console.log('before html push htmlObj', htmlObj);

          html.push(
            smtp.createHTMLResultsForEmail(htmlObj)
          );

          attachments.push({
            filename: getFilename(schedule.emailSchedules[i].type.toLowerCase(), fileSuffix),
            content: csvResults[i],
            contentType: 'text/csv'
          });

          emailSubjects.push(emailSubject);
        }
        catch (ex) {
          console.log('csv failed to send', ex);
        }
      }
      console.log('after calls html', html);

      for (var i = 0; i < emails.length; ++i) {
        for (var j = 0; j < html.length; ++j) {
          // var rawText = mappedResults[j].length === 0 ? "No Results" : "";
          smtp.send("data@linescape.com", emails[i], emailSubjects[j], "", html[j], attachments[j]);
        }
      }
      resolve();
    }).catch(err => {
      console.log("error in calls");
      reject(err);
    });
  });
};

var getFilename = (emailScheduleType, suffix) => {
  var date = moment().format("YYYY-MM-DD");

  return `Linescape_${date}_${emailScheduleType.toLowerCase()}_${suffix}.csv`;
};

var getEmailScheduleContent = schedule => {
  var content = [];
  var calls = [];
  var date = moment().format("YYYY-MM-DD");
  var end = moment().add(1, 'month').format("YYYY-MM-DD");

  for (var i = 0; i < schedule.emailSchedules.length; ++i) {
    var emailSchedule = schedule.emailSchedules[i];
    switch (emailSchedule.type.toLowerCase()) {
      case "port":
        calls.push(api.calls({
          "eta.gte": date,
          "etd.lte": end,
          "port_id": emailSchedule.portId,
          limit: 200
        }));
        break;
      case "portpair":
        calls.push(api.trips({
          "start_date.gte": date,
          "start_date.lte": end,
          "origin_ids": emailSchedule.originPortId,
          "destination_ids": emailSchedule.destinationPortId,
          limit: 200
        }));
        break;
      case "vessel":
        calls.push(api.calls({
          "eta.gte": date,
          "etd.lte": end,
          "vessel_id": emailSchedule.vesselId,
          limit: 200
        }));
        break;
    }
  }

  return calls;
};

module.exports.generateSitemap = (event, context, callback) => {
  // api.ports({
  //   limit: 2000
  // }).then(result => {
  //   let ports = result[0];

  //   for (var i = 0; i < ports.items.length; ++i) {
  //     var port = ports.items[i];

  //     sitemap.add({url: 'ports/' + port.id});
  //   }
  // });

  // api.carriers({
  //   limit: 2000
  // }).then(result => {
  //   let carriers = result[0];

  //   for (var i = 0; i < carriers.items.length; ++i) {
  //     var carrier = carriers.items[i];

  //     sitemap.add({url: 'carriers/' + carrier.id});
  //   }
  // });

  // api.vessels({
  //   limit: 2000
  // }).then(result => {
  //   let vessels = result[0];

  //   for (var i = 0; i < vessels.items.length; ++i) {
  //     var vessels = vessels.items[i];

  //     sitemap.add({url: 'vessels/' + vessel.id});
  //   }
  // });

  // save sitemap to s3
  callback(null, null);
};