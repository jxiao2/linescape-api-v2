"use strict";

import AWS from "aws-sdk";
import https from "https";
import dynamo from "../dynamo";
import api from "../lshift-api";
import Promise from 'bluebird';
import utils from "../utils";
import Error from "../models/error.js";
import stripeLib from "stripe";
const stripe = stripeLib(process.env.STRIPE_ID);

/**
 * Create stripe customer.
 */
module.exports.createCustomer = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  console.log('createCustomer event data', event.data);
  stripe.customers.create({
    description: 'Customer for ' + event.data.email,
    email: event.data.email,
    source: event.data.source
  }).then((customer) => {
    console.log('stripe customers.create callback', customer);
    dynamo.addStripeId(cognitoId, customer.id).then(response => {
      callback(null, customer);
    }).catch(err => {
      console.log('dynamo addStripeId error', err);
      var error = new Error(400, null, null, err);
      callback(JSON.stringify(error));
    });
    callback(null, customer);
  }).catch((err) => {
    console.log('stripe customers.create error', err);
    var error = new Error(400, null, null, err);
    callback(JSON.stringify(error));
  });
};

module.exports.confirmUser = (userIdentityId, profile) => {
  console.log('confirm', 11, 11, profile);
  let email = profile.email;
  let token = profile.account.token;
  let coupon = profile.account.coupon;
  let planId = profile.account.planId;
  console.log('confirm', 11);
  let customerParams = {
    description: 'Customer for ' + email,
    email: email
  };

  if (token != '' && token != undefined && token != null) {
    customerParams.source = token;
  }

  return new Promise((resolve, reject) => {
    stripe.customers.create(
      customerParams
    ).then((customer) => {
      console.log('confirm', 12, customer);
      let stripeId = customer.id;
      dynamo.addStripeId(userIdentityId, stripeId).then(response => {
        console.log('confirm', 13);
        let params = {
          plan: planId,
          customer: stripeId
        };
        console.log('before coupon code', params, coupon);
        if (coupon != '' && coupon != undefined && coupon != null) {
          params.coupon = coupon;
        }

        console.log('after coupon code');
        stripe.subscriptions.create(params).then((subscription) => {
          console.log('confirm', 14);
          dynamo.updateUserPlan(userIdentityId, stripeId, subscription.plan)
            .then((result) => {
              console.log('confirm', 15);
              resolve(null, subscription);
            }).catch(err => {
              console.log('confirm', 16);
              // need to rollback stripe subscription if you cannot update the plan in the db.
              // not sure about the error message
              reject(err);
            });
        }).catch(err => {
          console.log('confirm', 17);
          reject(err);
        });
      }).catch(err => {
        console.log('confirm', 18, err);
        reject(err);
      });
    }).catch(err => {
      console.log('confirm', 19, err);
      reject(err);
    });
  });
};

/**
 * Subscribe customer to a plan.
 */
// module.exports.createSubscription = (event, context, callback) => {
//   var cognitoId = utils.getCognitoId(event);

//   dynamo.getUserStripeId(cognitoId)
//     .then(stripeId => {
//       console.log('createSubscription before calling stripe', stripeId);
//       stripe.subscriptions.create({
//         customer: stripeId,
//         plan: event.data.plan
//       }).then((subscription) => {
//         dynamo.updateUserPlan(cognitoId, stripeId, subscription.plan)
//           .then(() => {
//             callback(null, subscription);
//           }).catch(err => {
//             // need to rollback stripe subscription if you cannot update the plan in the db.
//             // not sure about the error message
//             var error = new Error(400,
//               "A validation error occurred updating the database. See the details for more information.",
//               "1",
//               err);
//             callback(JSON.stringify(error));
//           });
//       }).catch((err) => {
//         callback(JSON.stringify(err));
//       });
//     }).catch(err => {
//       var error = new Error(400, null, null, err);
//       callback(JSON.stringify(error));
//     });
// };

/**
 * Subscribe customer to a plan.
 */
module.exports.subscribe = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  console.log('subscribe', event);
  dynamo.getUserStripeId(cognitoId)
    .then(stripeId => {
      stripe.customers.retrieve(
        stripeId
      ).then((customer) => {
        setSubscription(cognitoId, customer, event).then(subscription => {
          callback(null, subscription);
        }).catch(error => {
          callback(error);
        });
      }).catch((err) => {
        if (event.data.coupon !== undefined && event.data.coupon !== null && event.data.coupon.length !== 0) {
          setCustomer(cognitoId, event.data.coupon).then(customer => {
            setSubscription(cognitoId, customer, event).then(subscription => {
              callback(null, subscription);
            }).catch(error => {
              callback(error);
            });
          }).catch(err => {
            var error = new Error(400, null, null, err);
            callback(JSON.stringify(error));
          });
        } else {
          var error = new Error(400, null, null, err);
          callback(JSON.stringify(err));
        }
      });
    }).catch(err => {
      var error = new Error(400, null, null, err);
      callback(JSON.stringify(error));
    });
};

var setCustomer = (cognitoId, coupon) => {
  console.log('setCustomer', coupon);
  return new Promise((resolve, reject) => {
    dynamo.getUserProfile(cognitoId).then(profile => {
      console.log('getUserProfile', profile);
      let customerParams = {
        description: 'Customer for ' + profile.email,
        email: profile.email
      };
      console.log('before stripe call', customerParams);
      stripe.customers.create(
        customerParams
      ).then((customer) => {
        let stripeId = customer.id;
        dynamo.addStripeId(cognitoId, stripeId).then(response => {
          resolve(customer);
        }).catch(err => {
          reject(err);
        });
      }).catch(err => {
        reject(err);
      });
    }).catch(err => {
      reject(err);
    });
  });
}

var setSubscription = (cognitoId, customer, event) => {
  return new Promise((resolve, reject) => {
    console.log('setSubscription customer', customer);
    if (customer.subscriptions.total_count !== 0) {
      let subscriptionId = customer.subscriptions.data[0].id;
      let params = {
        plan: event.data.plan,
        trial_end: 'now'
      };

      if (event.data.coupon !== undefined && event.data.coupon !== null && event.data.coupon.length !== 0) {
        params.coupon = event.data.coupon;
      }

      if (customer.default_source !== null && customer.default_source !== undefined) {
        params.source = customer.default_source;
      }
      console.log('subscriptions update', params);
      stripe.subscriptions.update(
        subscriptionId,
        params
      ).then((subscription) => {
        dynamo.updateUserPlan(cognitoId, customer.id, subscription.plan)
        .then(() => {
          resolve(subscription);
        }).catch(err => {
          var error = new Error(400,
          "A validation error occurred updating the database. See the details for more information",
          "1",
          err);
          reject(JSON.stringify(error));
        });
      }).catch((err) => {
        var error = new Error(400, null, null, err);
        reject(JSON.stringify(error));
      });
    }
    else {
      let params = {
        plan: event.data.plan,
        customer: customer.id
      };

      if (event.data.coupon !== undefined && event.data.coupon !== null && event.data.coupon.length !== 0) {
        params.coupon = event.data.coupon;
      }

      if (customer.default_source !== null && customer.default_source !== undefined) {
        params.source = customer.default_source;
      }

      console.log('subscriptions set', params);
      stripe.subscriptions.create(params).then((subscription) => {
        console.log('stripe subscription created', subscription);
        dynamo.updateUserPlan(cognitoId, customer.id, subscription.plan)
          .then(() => {
            resolve(subscription);
          }).catch(err => {
            // need to rollback stripe subscription if you cannot update the plan in the db.
            // not sure about the error message
            var error = new Error(400,
              "A validation error occurred updating the database. See the details for more information.",
              "1",
              err);
            reject(JSON.stringify(error));
          });
      }).catch((err) => {
        var error = new Error(400, null, null, err);
        reject(JSON.stringify(error));
      });
    }
  });
}

/**
 * Update customer's plan.
 */
// module.exports.updateSubscription = (event, context, callback) => {
//   var cognitoId = utils.getCognitoId(event);

//   dynamo.getUserStripeId(cognitoId)
//     .then(stripeId => {
//       stripe.subscriptions.update(
//         stripeId,
//         { plan: event.data.plan },
//         function(err, subscription) {
//           if (err) {
//             callback(JSON.stringify(err));
//           }
//           dynamo.updateUserPlan(cognitoId, stripeId, subscription.plan)
//             .then(() => {
//               callback(null, subscription);
//             }).catch(err => {
//               // need to rollback stripe subscription if you cannot update the plan in the db.
//               // not sure avout the error message
//               var error = new Error(400,
//                 "A validation error occurred updating the database. See the details for more information.",
//                 "1",
//                 err);
//               callback(JSON.stringify(error));
//             });
//         });
//     }).catch(err => {
//       var error = new Error(400, null, null, err);
//       callback(JSON.stringify(error));
//     });
// };

/**
 * Return (limit 4) of the stripe plans.
 */
module.exports.getPlans = (event, context, callback) => {
  // var cognitoId = utils.getCognitoId(event);

  stripe.plans.list({
    limit: 6
  }).then((plans) => {
    callback(null, plans);
  }).catch((err) => {
    callback(JSON.stringify(err));
  });
};

/**
 * Return the customer's stripe details.
 */
module.exports.getCustomer = (event, context, callback) => {
  console.log('getCustomer event data', event.data);
  var cognitoId = utils.getCognitoId(event);

  dynamo.getUserStripeId(cognitoId)
    .then(stripeId => {
      console.log('getCustomer stripeId', stripeId);
      stripe.customers.retrieve(
        stripeId
      ).then((customer) => {
        console.log('getCustomer customer', customer);
        callback(null, customer);
      }).catch((err) => {
        callback(JSON.stringify(err));
      });
    }).catch(err => {
      var error = new Error(400, null, null, err);
      callback(JSON.stringify(error));
    });
};

/**
 * Checks if the provided Coupon is valid
 */
module.exports.checkCoupon = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);

  var query = JSON.parse(event.query);
  var couponQuery = query.coupon;

  stripe.coupons.retrieve(
    couponQuery
  ).then((coupon) => {
    callback(null, coupon);
  }).catch((err) => {
    // coupon is not valid
    callback(JSON.stringify(err));
  });
};

/**
 * Updates the customer's card details.
 * might need to be renamed to updateCard
 * CHECK PUT RESPONSES: 201 indicates card should be created if it does not exist
 */

module.exports.updateCustomer = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);

  dynamo.getUserStripeId(cognitoId)
    .then(stripeId => {
      stripe.customers.update(
        stripeId, {
        source: event.data.source
      }).then((customer) => {
        callback(null, customer);
      }).catch(err => {
        var error = new Error(400, null, null, err);
        callback(JSON.stringify(error));
      });
    }).catch(err => {
      var error = new Error(400, null, null, 'not a current customer');
      callback(JSON.stringify(error));
    });
};

/**
 * Cancels the customer's subscription.
 */

module.exports.cancelSubscription = (event, context, callback) => {
  console.log('cancelSubscription event', event);
  var cognitoId = utils.getCognitoId(event);
  // want to make sure client has to know the subscription (ie show the customer their details before letting them cancel it)
  console.log('cancelSubscription cognitoId', cognitoId);
  dynamo.getUserStripeId(cognitoId)
  .then(stripeId => {
    console.log('cancelSubscription stripeId', stripeId);
    stripe.subscriptions.list({
      customer: stripeId,
      limit: 1
    }).then((subscriptions) => {
      console.log('cancelSubscription subscriptions', subscriptions);
      let subscription = subscriptions.data[0];
      console.log('cancelSubscription subscriptionId', subscription.id);
      if (subscription !== null) {
        stripe.subscriptions.del(
          subscription.id,
          { at_period_end: 'false' }
        ).then(response => {
          console.log('cancelSubscription del', subscriptions);
          let plan = {
            id: null,
            name: null,
            amount: null,
            interval: null,
            trial_period_days: null
          };

          dynamo.updateUserPlan(cognitoId, stripeId, plan).then(account => {
            console.log('cancelSubscription account', account);
            callback(null, account);
          }).catch(err => {
            var error = new Error(400, null, null, err);
            callback(JSON.stringify(error));
          });
        }).catch(err => {
          var error = new Error(400, null, null, err);
          callback(JSON.stringify(error));
        });
      }
    }).catch(err => {
      var error = new Error(400, null, null, err);
      callback(JSON.stringify(error));
    });
  }).catch(err => {
    var error = new Error(400, null, null, err);
    callback(JSON.stringify(error));
  });

  // right now... thinking. cancel subscription, leave it going for now, and stop it when the webhook happens
};

/**
 * Retrieves the customer's stripe invoices.
 */
module.exports.getInvoices = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);

  dynamo.getUserStripeId(cognitoId)
    .then(stripeId => {
      console.log('get stripe invoices stripeId!', stripeId);
      if (stripeId == null) {
        console.log('stripe is undefined, do not call stripe');
        var error = new Error(400, null, null, "no invoices");
        callback(JSON.stringify(error));
      }
      else {
        stripe.invoices.list({
            customer: stripeId
          }).then((invoices) => {
            callback(null, invoices.data);
          }).catch( err => {
            callback(JSON.stringify(err));
          });
      }
    }).catch(err => {
      var error = new Error(400, null, null, err);
      callback(JSON.stringify(error));
    });
};

/**
 * Stripe Webhook endpoint for invoice.payment_failed
 */
module.exports.paymentFailed = (event, context, callback) => {
  if (event.data !== null) {
    console.log(event.data.data.object);
  }

  // event.data.data.object
};

/**
 * Stripe Webhook endpoint for invoice.payment_succeeded
 */
module.exports.paymentSucceeded = (event, context, callback) => {
  if (event.data !== null) {
    console.log(event.data.data.object);
  }
};

/**
 * Stripe Webhook endpoint for customer.subscription.deleted
 */
module.exports.subscriptionDeleted = (event, context, callback) => {
  if (event.data !== null) {
    console.log(event.data);
  }

  var customerId = event.data.data.customer;
  dynamo.getCognitoIdFromStripeId(customerId)
    .then(response => {
      // should use response
      response = utils.getCognitoId(event);
      dynamo.updateUserPlan(response, "free")
        .then(() => {
          callback(null, null);
        }).catch(err => {
          callback(JSON.stringify(err));
        });
    }).catch(err => {
      var error = new Error(400, "That stripe id does not exist in our records", "1", err);
      callback(JSON.stringify(error));
    });
};