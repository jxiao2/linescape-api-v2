"use strict";

import AWS from "aws-sdk";
import https from "https";
import dynamo from "../dynamo";
import api from "../lshift-api";
import Promise from 'bluebird';
import utils from "../utils";
import Error from "../models/error.js";
import momentTimezone from "moment-timezone";
import _ from 'lodash';
import errorCodes from "../globals/errorCodes.js";
import accountTypes from "../globals/accountTypes.js";

/**
 * Retrieves locations.
 */
module.exports.locations = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  var query = JSON.parse(event.query);
  var queryObject = utils.getSearchObjectFromQuery(query, "location");

  dynamo.recordSearch(cognitoId, queryObject, "location", event.path).then(id => {
    api.locations(queryObject).then(locations => {
      locations.searchId = id;
      callback(null, locations);
    }).catch(err => {
      var error = new Error(400, null, null, err);
      callback(JSON.stringify(error));
    });
  });
};

/**
 * Retrieves ports.
 */
module.exports.ports = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  var query = JSON.parse(event.query);
  var queryObject = utils.getSearchObjectFromQuery(query, "port");
  var calls = [
    api.ports(queryObject),
    dynamo.getUserFavorites(cognitoId)
  ];

  dynamo.getUserAccountType(cognitoId).then((accountType) => {
    dynamo.recordSearch(cognitoId, queryObject, "port", event.path).then(id => {
      Promise.all(calls).then((result) => {
        var accountTypeId = accountType.id;
        var favorites = result[1];
        var ports = result[0];
        var hasFavorites = favorites &&
          typeof favorites.favorites !== 'undefined' &&
          typeof favorites.favorites.ports !== 'undefined';

        if (hasFavorites) {
          var favoritePorts = favorites.favorites.ports.values;
          for (var i = 0; i < ports.items.length; ++i) {
            var port = ports.items[i];

            ports.items[i].userData = utils.getUserData(
              favoritePorts.indexOf(port.id) !== -1,
              ''
            );
          }
        }
      
        ports.searchId = id;
        callback(null, ports);
      }).catch(err => {
        var error = new Error(400, null, null, err);
        callback(JSON.stringify(error));
      });
    });
  }).catch(err => {
    var error = new Error(400, "accountType not found", null,
      err);
    callback(JSON.stringify(error));
  });
};

/**
 * Retrieves schedules for a given port.
 */
module.exports.getSchedulesByPort = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  var portId = 34761;//utils.getParam(event.params, "portId", "int", event.path);
  var query = JSON.parse(event.query);

  console.log("query", event.query);
  var queryObject = utils.getSearchObjectFromQuery(query, "portSchedule", {
    port_id: portId
  });

  var calls = [
    api.calls(queryObject, portId),
    dynamo.getUserFavorites(cognitoId)
  ];

  dynamo.incrementUserSearchesOrRejectOnLimitExceeded(cognitoId).then((accountType) => {
    dynamo.recordSearch(cognitoId, queryObject, "portSchedule", event.path).then(id => {
      Promise.all(calls).then(result => {
        var accountTypeId = accountType.id;
        var favorites = result[1];
        var schedules = result[0];
        var hasFavorites = favorites &&
          typeof favorites.favorites !== 'undefined';

        var hasCarrierFavorites = hasFavorites && !_.isNil(favorites.favorites.carriers);
        var hasVesselFavorites = hasFavorites && !_.isNil(favorites.favorites.vessels);
        var hasPortFavorites = hasFavorites && !_.isNil(favorites.favorites.ports);

        if (hasPortFavorites) {
          schedules.userData = utils.getUserData(
            favorites.favorites.ports.values.indexOf(portId) !== -1,
            ''
          );
        }

        if (hasCarrierFavorites) {
          for (var i = 0; i < schedules.summary.available_carriers.length; ++i) {
            var carrierLoc = schedules.summary.available_carriers[i];
            schedules.summary.available_carriers[i].carrier.userData = utils.getUserData(
              favorites.favorites.carriers.values.indexOf(carrierLoc.carrier.id) !== -1,
              ''
            );
          }
        }
        for (var i = 0; i < schedules.items.length; ++i) {
          schedules.items[i].carrier = utils.obfuscateResource(accountTypeId, schedules.items[i].carrier, "carrier");
          schedules.items[i].vessel = utils.obfuscateResource(accountTypeId, schedules.items[i].vessel, "vessel");
          var carrier = schedules.items[i].carrier;
          var vessel = schedules.items[i].vessel;

          if (hasCarrierFavorites) {
            schedules.items[i].carrier.userData = utils.getUserData(
              favorites.favorites.carriers.values.indexOf(carrier.id) !== -1,
              ''
            );
          }
          else {
            schedules.items[i].carrier.userData = utils.getUserData(
              false,
              ''
            );
          }

          if (hasVesselFavorites) {
            schedules.items[i].vessel.userData = utils.getUserData(
              favorites.favorites.vessels.values.indexOf(vessel.id) !== -1,
              ''
            );
          }
          else {
            schedules.items[i].vessel.userData = utils.getUserData(
              false,
              ''
            );
          }

          // obfuscate date
          schedules.items[i].eta = utils.obfuscateResource(accountTypeId, schedules.items[i].eta, "date");
          schedules.items[i].eta_time = utils.obfuscateResource(accountTypeId, schedules.items[i].eta_time, "time");
          schedules.items[i].etd = utils.obfuscateResource(accountTypeId, schedules.items[i].etd, "date");
          schedules.items[i].etd_time = utils.obfuscateResource(accountTypeId, schedules.items[i].etd_time, "time");
        }

        schedules.items = utils.obfuscateResources(accountTypeId, schedules.items, "schedule");
        schedules.searchId = id;
        callback(null, schedules);
      }).catch(err => {
        var error = new Error(400, null, null, err);
        callback(JSON.stringify(error));
      });
    });
  }).catch(err => {
    var error = new Error(429,
      "Search limit exceeded",
      errorCodes.searchLimitExceeded,
      err);
    callback(JSON.stringify(error));
  });
};

/**
 * Searches for trips
 */
module.exports.trips = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  var query = JSON.parse(event.query);
  var queryObject = utils.getSearchObjectFromQuery(query, "trip");

  var calls = [
    api.trips(queryObject),
    dynamo.getUserFavorites(cognitoId)
  ];

  dynamo.incrementUserSearchesOrRejectOnLimitExceeded(cognitoId).then((accountType) => {
    dynamo.recordSearch(cognitoId, queryObject, "trip", event.path).then(id => {
      var accountTypeId = accountType.id;
      Promise.all(calls).then(result => {
        try {
          var trips = result[0];
          var favorites = result[1];


          var hasFavorites = favorites &&
            typeof favorites.favorites !== 'undefined';

          var hasCarrierFavorites = hasFavorites && !_.isNil(favorites.favorites.carriers);
          var hasVesselFavorites = hasFavorites && !_.isNil(favorites.favorites.vessels);
          var hasPortFavorites = hasFavorites && !_.isNil(favorites.favorites.ports);

          for (let i = 0; i < trips.summary.available_origin_ports.length; ++i) {
            if (hasPortFavorites) {
              let port = trips.summary.available_origin_ports[i].port;
              trips.summary.available_origin_ports[i].port.userData = utils.getUserData(
                favorites.favorites.ports.values.indexOf(port.id) !== -1,
                ''
              );
            }
            else {
              trips.summary.available_origin_ports[i].port.userData = utils.getUserData(
                false,
                ''
              );
            }
          }

          for (let i = 0; i < trips.summary.available_destination_ports.length; ++i) {
            if (hasPortFavorites) {

              let port = trips.summary.available_destination_ports[i].port;
              trips.summary.available_destination_ports[i].port.userData = utils.getUserData(
                favorites.favorites.ports.values.indexOf(port.id) !== -1,
                ''
              );
            }
            else {
              trips.summary.available_destination_ports[i].port.userData = utils.getUserData(
                false,
                ''
              );
            }
          }

          if (hasCarrierFavorites) {
            for (let i = 0; i < trips.summary.available_carriers.length; ++i) {
              let carrierLoc = trips.summary.available_carriers[i];
              trips.summary.available_carriers[i].carrier.userData = utils.getUserData(
                favorites.favorites.carriers.values.indexOf(carrierLoc.carrier.id) !== -1,
                ''
              );
            }
          }

          for (let i = 0; i < trips.items.length; ++i) {
            trips.items[i].carrier = utils.obfuscateResource(accountTypeId, trips.items[i].carrier, "carrier");

            var carrier = trips.items[i].carrier;

            if (hasCarrierFavorites) {
              trips.items[i].carrier.userData = utils.getUserData(
                favorites.favorites.carriers.values.indexOf(carrier.id) !== -1,
                ''
              );
            }
            else {
              trips.items[i].carrier.userData = utils.getUserData(
                false,
                ''
              );
            }

            let legs = trips.items[i].legs;
            for (let j = 0; j < legs.length; ++j) {

              if (hasPortFavorites) {
                legs[j].end.port.userData = utils.getUserData(
                  favorites.favorites.ports.values.indexOf(legs[j].end.port.id) !== -1,
                  ''
                );
                legs[j].start.port.userData = utils.getUserData(
                  favorites.favorites.ports.values.indexOf(legs[j].start.port.id) !== -1,
                  ''
                );
              }
              else {
                legs[j].start.port.userData = utils.getUserData(
                  false,
                  ''
                );
                legs[j].end.port.userData = utils.getUserData(
                  false,
                  ''
                );
              }

              try {
                // obfuscate vessel
                if (legs.length !== 1 && (accountTypeId === accountTypes.basic || accountTypeId === accountTypes.unregistered)) {
                  legs[j].vessel = {
                    id: 0,
                    imo: 'blurred text',
                    name: 'blurred text'
                  };
                }
              }
              catch (ex) {
              }

              if (legs.length !== 1 && (accountTypeId === accountTypes.basic || accountTypeId === accountTypes.unregistered)) {
                legs[j].voyage = 'blurred text';
                legs[j].schedule_id = 'blurred text';
              }

              // obfuscate route
              if (legs.length !== 1 && (accountTypeId === accountTypes.basic || accountTypeId === accountTypes.unregistered)) {
                legs[j].route = {
                  code: 'blurred text',
                  id: 0,
                  name: 'blurred text'
                };
              }

              // obfuscate date
              console.log('obfuscate date start', legs[j].start);
              console.log('obfuscate date end', legs[j].end);
              legs[j].start.date = utils.obfuscateResource(accountTypeId, legs[j].start.date, 'date');
              legs[j].start.time = utils.obfuscateResource(accountTypeId, legs[j].start.time, 'time');
              legs[j].end.date = utils.obfuscateResource(accountTypeId, legs[j].end.date, 'date');
              legs[j].end.time = utils.obfuscateResource(accountTypeId, legs[j].end.time, 'time');

              if (legs.length !== 1 && (accountTypeId === accountTypes.basic || accountTypeId === accountTypes.unregistered)) {
                if (j === 0) {
                  // first leg
                  legs[j].end = {
                    date: 'blurred text',
                    port: {
                      code: 'blurred text',
                      id: 0,
                      name: 'blurred text',
                      country: {
                        id: 'blurred text',
                        name: 'blurred text'
                      },
                      latlong: [
                        null, null
                      ],
                      region: {
                        id: 0,
                        name: 'blurred text',
                        country_id: 'blurred text'
                      },
                      userData: {
                        favorited: false,
                        note: ''
                      }
                    }
                  };
                }
                else if (j === legs.length - 1) {
                  legs[j].start = {
                    date: 'blurred text',
                    port: {
                      code: 'blurred text',
                      id: 0,
                      name: 'blurred text',
                      country: {
                        id: 'blurred text',
                        name: 'blurred text'
                      },
                      latlong: [
                        null, null
                      ],
                      region: {
                        id: 0,
                        name: 'blurred text',
                        country_id: 'blurred text'
                      },
                      userData: {
                        favorited: false,
                        note: ''
                      }
                    }
                  };
                }
                else {
                  legs[j].start = {
                    date: 'blurred text',
                    port: {
                      code: 'blurred text',
                      id: 0,
                      name: 'blurred text',
                      country: {
                        id: 'blurred text',
                        name: 'blurred text'
                      },
                      latlong: [
                        null, null
                      ],
                      region: {
                        id: 0,
                        name: 'blurred text',
                        country_id: 'blurred text'
                      },
                      userData: {
                        favorited: false,
                        note: ''
                      }
                    }
                  };

                  legs[j].end = {
                    date: 'blurred text',
                    port: {
                      code: 'blurred text',
                      id: 0,
                      name: 'blurred text',
                      country: {
                        id: 'blurred text',
                        name: 'blurred text'
                      },
                      latlong: [
                        null, null
                      ],
                      region: {
                        id: 0,
                        name: 'blurred text',
                        country_id: 'blurred text'
                      },
                      userData: {
                        favorited: false,
                        note: ''
                      }
                    }
                  };                    
                }
              }

            }
          }
          trips.searchId = id;
          trips.items = utils.obfuscateResources(accountTypeId, trips.items, "schedule");

          callback(null, trips);
        }
        catch (ex) {
          let error = new Error(400, null, null, ex);

          if (result[0].summary === undefined || result[0].summary === null) {
            error = new Error(504, "Data API Timeout", errorCodes.timeoutError, result.errorMessage);
          }
          callback(JSON.stringify(error));
        }
      }).catch(err => {
        var error = new Error(400, null, null, err);
        callback(JSON.stringify(error));
      });
    });
  }).catch(err => {
    var error = new Error(429,
      "Search limit exceeded",
      errorCodes.searchLimitExceeded,
      err);
    callback(JSON.stringify(error));
  });

};

/**
 * Searches for schedules
 */
module.exports.schedules = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  var query = JSON.parse(event.query);
  var queryObject = utils.getSearchObjectFromQuery(query, "schedule");

  dynamo.incrementUserSearchesOrRejectOnLimitExceeded(cognitoId).then((accountType) => {
    dynamo.recordSearch(cognitoId, queryObject, "schedule", event.path).then(id => {
      var accountTypeId = accountType.id;
      api.schedules(queryObject, null, "").then(locations => {
        locations.searchId = id;
        locations.items = utils.obfuscateResources(accountTypeId, locations.items, "schedule");

        callback(null, locations);
      }).catch(err => {
        var error = new Error(400, null, null, err);
        callback(JSON.stringify(error));
      });
    });
  }).catch(err => {
    var error = new Error(429,
      "Search limit exceeded",
      errorCodes.searchLimitExceeded,
      err);
    callback(JSON.stringify(error));
  });
};


/**
 * Retrieves carriers.
 */
module.exports.carriers = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  var query = JSON.parse(event.query);
  var queryObject = utils.getSearchObjectFromQuery(query, "carrier");

  var calls = [
    api.carriers(queryObject),
    dynamo.getUserFavorites(cognitoId)
  ];

  dynamo.getUserAccountType(cognitoId).then((accountType) => {
    dynamo.recordSearch(cognitoId, queryObject, "carrier", event.path).then(id => {
      Promise.all(calls).then(results => {
        var accountTypeId = accountType.id;
        var carriers = results[0];
        var favorites = results[1];

        var hasFavorites = favorites && typeof favorites.favorites !== 'undefined';

        if (hasFavorites) {
          var hasCarrierFavorites = typeof favorites.favorites.carriers !== 'undefined';

          for (var i = 0; i < carriers.items.length; ++i) {
            var carrier = carriers.items[i];

            if (hasCarrierFavorites) {
              carriers.items[i].userData = utils.getUserData(
                favorites.favorites.carriers.values.indexOf(carrier.id) !== -1,
                ''
              );
            }
            else {
              carriers.items[i].userData = utils.getUserData(
                false,
                ''
              );
            }
          }
        }
        carriers.items = utils.obfuscateResources(accountTypeId, carriers.items, "carrier");
        carriers.searchId = id;
        callback(null, carriers);
      }).catch(err => {
        var error = new Error(400, null, null, err);
        callback(JSON.stringify(error));
      });
    });

  }).catch(err => {
    var error = new Error(400, "accountType not found", null,
      err);
    callback(JSON.stringify(error));
  });
};


/**
 * Retrieves schedules for a given vessel.
 */
module.exports.getSchedulesByVessel = (event, context, callback) => {
  var vesselId = utils.getParam(event.params, "vesselId", "int");
  var cognitoId = utils.getCognitoId(event);
  var query = JSON.parse(event.query);
  var queryObject = utils.getSearchObjectFromQuery(query, "vesselSchedule", {
    vessel_id: vesselId
  });

  var calls = [
    api.calls(queryObject, vesselId, "vessel"),
    dynamo.getUserFavorites(cognitoId)
  ];

  dynamo.incrementUserSearchesOrRejectOnLimitExceeded(cognitoId).then((accountType) => {
    dynamo.recordSearch(cognitoId, queryObject, "vesselSchedule", event.path).then(id => {
      Promise.all(calls).then(result => {
        var accountTypeId = accountType.id;
        var schedules = result[0];
        var favorites = result[1];

        var hasFavorites = favorites && typeof favorites.favorites !== 'undefined';
        var hasCarrierFavorites = hasFavorites && !_.isNil(favorites.favorites.carriers);
        // vessel favorites not necessary as it should be the same vessel
        var hasPortFavorites = hasFavorites && !_.isNil(favorites.favorites.ports);

        if (hasCarrierFavorites) {
          for (let i = 0; i < schedules.summary.available_carriers.length; ++i) {
            var carrierLoc = schedules.summary.available_carriers[i];
            schedules.summary.available_carriers[i].carrier.userData = utils.getUserData(
              favorites.favorites.carriers.values.indexOf(carrierLoc.carrier.id) !== -1,
              ''
            );
          }
        }

        for (let i = 0; i < schedules.items.length; ++i) {
          schedules.items[i].carrier = utils.obfuscateResource(accountTypeId, schedules.items[i].carrier, "carrier");
          schedules.items[i].port = utils.obfuscateResource(accountTypeId, schedules.items[i].port, "vessel");
          var carrier = schedules.items[i].carrier;
          var port = schedules.items[i].port;

          if (hasCarrierFavorites) {
            schedules.items[i].carrier.userData = utils.getUserData(
              favorites.favorites.carriers.values.indexOf(carrier.id) !== -1,
              ''
            );
          }
          else {
            schedules.items[i].carrier.userData = utils.getUserData(
              false,
              ''
            );
          }

          if (hasPortFavorites) {
            schedules.items[i].port.userData = utils.getUserData(
              favorites.favorites.ports.values.indexOf(port.id) !== -1,
              ''
            );
          }
          else {
            schedules.items[i].vessel.userData = utils.getUserData(
              false,
              ''
            );
          }

          // obfuscate date
          schedules.items[i].eta = utils.obfuscateResource(accountTypeId, schedules.items[i].eta, "date");
          schedules.items[i].eta_time = utils.obfuscateResource(accountTypeId, schedules.items[i].eta_time, "time");
          schedules.items[i].etd = utils.obfuscateResource(accountTypeId, schedules.items[i].etd, "date");
          schedules.items[i].etd_time = utils.obfuscateResource(accountTypeId, schedules.items[i].etd_time, "time");
        }
        schedules.items = utils.obfuscateResources(accountTypeId, schedules.items, "schedule");

        schedules.searchId = id;
        callback(null, schedules);
      }).catch(err => {
        var error = new Error(400, null, null, err);
        callback(JSON.stringify(error));
      });
    });
  }).catch(err => {
    var error = new Error(429,
      "Search limit exceeded",
      errorCodes.searchLimitExceeded,
      err);
    callback(JSON.stringify(error));
  });
};

/**
 * Retrieves local contacts for a given carrier.
 */
module.exports.getLocalContactsByCarrier = (event, context, callback) => {
  var carrierId = utils.getParam(event.params, "carrierId", "int");
  var cognitoId = utils.getCognitoId(event);
  var query = JSON.parse(event.query);
  var queryObject = utils.getSearchObjectFromQuery(query, "localcontact", {
    carrier_id: carrierId
  });

  dynamo.incrementUserSearchesOrRejectOnLimitExceeded(cognitoId).then(() => {
    dynamo.recordSearch(cognitoId, queryObject, "localcontact", event.path).then(id => {
      api.localcontacts(queryObject).then(contacts => {
        contacts.searchId = id;
        callback(null, contacts);
      }).catch(err => {
        var error = new Error(400, null, null, err);
        callback(JSON.stringify(error));
      });
    });
  }).catch(err => {
    var error = new Error(429,
      "Search limit exceeded",
      errorCodes.searchLimitExceeded,
      err);
    callback(JSON.stringify(error));
  });
};

/**
 * Retrieves schedules for a given carrier.
 */
module.exports.getSchedulesByCarrier = (event, context, callback) => {
  var carrierId = utils.getParam(event.params, "carrierId", "int");
  var cognitoId = utils.getCognitoId(event);
  var query = JSON.parse(event.query);
  var queryObject = utils.getSearchObjectFromQuery(query, "schedule", {
    carrier_id: carrierId
  });

  var calls = [
    api.schedules(queryObject, carrierId, "carrier"),
    dynamo.getUserFavorites(cognitoId)
  ];

  dynamo.incrementUserSearchesOrRejectOnLimitExceeded(cognitoId).then((accountType) => {
    dynamo.recordSearch(cognitoId, queryObject, "carrierSchedule", event.path).then(id => {
      Promise.all(calls).then(result => {
        var accountTypeId = accountType.id;
        var favorites = result[1];
        var items = result[0];

        var hasFavorites = favorites &&
          typeof favorites.favorites !== 'undefined';

        if (hasFavorites) {
          var hasCarrierFavorites = typeof favorites.favorites.carriers !== 'undefined';
          var hasVesselFavorites = typeof favorites.favorites.vessels !== 'undefined';
          var hasPortFavorites = typeof favorites.favorites.ports !== 'undefined';


          if (hasCarrierFavorites) {
            items.userData = utils.getUserData(
              favorites.favorites.carriers.values.indexOf(carrierId) !== -1,
              ''
            );
          }
          else {
            items.userData = utils.getUserData(
              false,
              ''
            );
          }

          for (var i = 0; i < items.items.length; ++i) {
            var startPort = items.items[i].start_port;
            var endPort = items.items[i].end_port;
            var vessel = items.items[i].vessel;

            if (hasVesselFavorites) {
              items.items[i].vessel.userData = utils.getUserData(
                favorites.favorites.vessels.values.indexOf(vessel.id) !== -1,
                ''
              );
            }
            else {
              items.items[i].vessel.userData = utils.getUserData(
                false,
                ''
              );
            }

            if (hasPortFavorites) {
              items.items[i].start_port.userData = utils.getUserData(
                favorites.favorites.ports.values.indexOf(startPort.id) !== -1,
                ''
              );
              items.items[i].end_port.userData = utils.getUserData(
                favorites.favorites.ports.values.indexOf(endPort.id) !== -1,
                ''
              );
            }
            else {
              items.items[i].start_port.userData = utils.getUserData(
                false,
                ''
              );
              items.items[i].end_port.userData = utils.getUserData(
                false,
                ''
              );
            }
          }
        }

        items.items = utils.obfuscateResources(accountTypeId, items.items, "schedule");

        items.searchId = id;
        callback(null, items);
      }).catch(err => {
        var error = new Error(400, null, null, err);
        callback(JSON.stringify(error));
      });
    });
  }).catch(err => {
    var error = new Error(429,
      "Search limit exceeded",
      errorCodes.searchLimitExceeded,
      err);
    callback(JSON.stringify(error));
  });
};

/**
 * Retrieves vessels.
 */
module.exports.vessels = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  var query = JSON.parse(event.query);
  var queryObject = utils.getSearchObjectFromQuery(query, "vessel");

  var calls = [
    api.vessels(queryObject),
    dynamo.getUserFavorites(cognitoId)
  ];

  dynamo.getUserAccountType(cognitoId).then((accountType) => {
    dynamo.recordSearch(cognitoId, queryObject, "vessel", event.path).then(id => {
      Promise.all(calls).then(result => {
        var accountTypeId = accountType.id;

        var vessels = result[0];
        var favorites = result[1];

        var hasFavorites = favorites && typeof favorites.favorites !== 'undefined';

        if (hasFavorites) {
          var hasVesselFavorites = typeof favorites.favorites.vessels !== 'undefined';
          var hasCarrierFavorites = typeof favorites.favorites.carriers !== 'undefined';


          for (var i = 0; i < vessels.items.length; ++i) {
            var vessel = vessels.items[i];
            var carrier = vessels.items[i].carrier;

            if (hasVesselFavorites) {
              vessels.items[i].userData = utils.getUserData(
                favorites.favorites.vessels.values.indexOf(vessel.id) !== -1,
                ''
              );
            }
            else {
              vessels.items[i].userData = utils.getUserData(
                false,
                ''
              );
            }

            if (carrier) {
              if (hasCarrierFavorites) {
                vessels.items[i].carrier.userData = utils.getUserData(
                  favorites.favorites.carriers.values.indexOf(carrier.id) !== -1,
                  ''
                );
              }
              else {
                vessels.items[i].carrier.userData = utils.getUserData(
                  false,
                  ''
                );
              }
            }
          }
        }

        vessels.items = utils.obfuscateResources(accountTypeId, vessels.items, "vessel");
        vessels.searchId = id;
        callback(null, vessels);
      }).catch(err => {
        var error = new Error(400, null, null, err);
        callback(JSON.stringify(error));
      });
    });
  }).catch(err => {
    var error = new Error(400, "accountType not found", null,
      err);
    callback(JSON.stringify(error));
  });
};