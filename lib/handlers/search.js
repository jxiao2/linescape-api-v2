"use strict";

import AWS from "aws-sdk";
import https from "https";
import dynamo from "../dynamo";
import api from "../lshift-api";
import Promise from "bluebird";
import utils from "../utils";
import smtp from "../utils/smtp.js";
import Error from "../models/error.js";
import errorCodes from "../globals/errorCodes.js";

/**
 * Downloads a search as CSV.
 */
module.exports.downloadSearch = (event, context, callback) => {
  var searchId = utils.getParam(event.params, "searchId", "string");

  dynamo.getSearchById(searchId).then(data => {
    var resultsPromise = getResultsPromise(data);
    resultsPromise.then(resultsToShow => {
      // CSV stuff
      var formattedData = utils.mapCsv(resultsToShow.items, data.searchType);
      var csv = utils.createCsv(formattedData).then(result => {
        var base64Result = new Buffer(result).toString('base64');
        callback(null, base64Result);
      });
    }).catch(err => {
      callback(err);
    });
  }).catch(err => {
    var error = new Error(500,
      "An unknown error occurred. See the details for more information.",
      errorCodes.serverError,
      err);
    callback(JSON.stringify(error));
  });
};

/**
 * Shares a search over email.
 */
module.exports.shareSearch = (event, context, callback) => {
  // do not care if the user is logged in or not
  console.log('event.data', event.data);
  var fromAddress = event.data.fromAddress;
  var toAddress = event.data.toAddress;
  var subject = event.data.subject || 'Someone has shared a Linescape search with you';
  var message = event.data.message || '';

  if (message.length > 1024) {
    var error = new Error(400,
    "A validation error occurred. See the details for more information.",
    errorCodes.validationError,
    "message length must not exceed 1024 characters.");
    callback(JSON.stringify(error));
    return;
  }

  var searchId = utils.getParam(event.params, "searchId", "string");
  dynamo.getSearchById(searchId).then(data => {
    var resultsPromise = getResultsPromise(data);

    resultsPromise.then(resultsToShow => {
      var formattedData = utils.mapCsv(resultsToShow.items, data.searchType);

      var htmlObj = {};
      var sts = standardizeSearchType(data.searchType);
      // setting type to share because share is only for trips at the moment
      htmlObj.share = formattedData;
      htmlObj.searchId = searchId;
      htmlObj.fromAddress = fromAddress;
      htmlObj.message = message;
      htmlObj.total = resultsToShow.summary.count;
      htmlObj.limit = htmlObj.total > 20 ? 20 : htmlObj.total;
      console.log('before createHTMLResultsForEmail', htmlObj);
      var html = smtp.createHTMLResultsForEmail(htmlObj);
      console.log('after createHTMLResultsForEmail', html);
      smtp.send('notifications@linescape.com', toAddress, subject, html, html);
      callback(null, null);
    }).catch(err => {
      var error = new Error(500,
        "An unknown error occurred. See the details for more information.",
        errorCodes.serverError,
        err);
      callback(JSON.stringify(error));
    });
  }).catch(err => {
    var error = new Error(500,
      "An unknown error occurred. See the details for more information.",
      errorCodes.serverError,
      err);
    callback(JSON.stringify(error));
  });
};

function standardizeSearchType(searchType) {
  switch (searchType.toLowerCase()) {
    case "carrier":
    case "port":
    case "vessel":
    case "location":
      return `${searchType.toLowerCase()}s`;
    case "trip":
    case "schedule":
    case "carrierschedule":
    case "portschedule":
    case "vesselschedule":
      return "schedules";
  }
}

function getResultsPromise(data) {
  switch (data.searchType.toLowerCase()) {
    case "schedule":
      return schedules(data.details);
    case "carrierschedule":
      return schedules(data.details);
    case "portschedule":
      return schedules(data.details);
    case "vesselschedule":
      return schedules(data.details);
    case "trip":
      return trips(data.details);
    case "carrier":
      return carriers(data.details);
    case "port":
      return ports(data.details);
    case "vessel":
      return vessels(data.details);
    case "location":
      return locations(data.details);
  }
}

function schedules(query, id, type) {
  return new Promise((resolve, reject) => {
    api.schedules(query, null, "").then(response => {
      resolve(response);
      /*var table = "";
      for (var current = 0; current < response.items.length; current++) {
        table += response.items[current].voyage + ", ";
      }
      table = table.substring(0, table.length - 2);
      resolve(table + '<br><br>' + JSON.stringify(response));*/
    }).catch(err => {
      var error = new Error(400, null, null, err);
      reject(JSON.stringify(error));
    });
  });
}

function trips(query) {
  return new Promise((resolve, reject) => {
    api.trips(query).then(response => {
      console.log("trips", response);
      resolve(response);
      /*
      var table = "";
      for (var current = 0; current < response.items.length; current++) {
        table += response.items[current] + ", ";
      }
      table = table.substring(0, table.length - 2);
      resolve(table + '<br><br>' + JSON.stringify(response));*/
    }).catch(err => {
      var error = new Error(400, null, null, "err");
      reject(JSON.stringify(error));
    });
  });
}

function carriers(query) {
  return new Promise((resolve, reject) => {
    api.carriers(query).then(response => {
      resolve(response);
      /*
            console.log("carriers", response);
            var table = "";
            for (var current = 0; current < response.items.length; current++) {
              table += response.items[current] + ", ";
            }
            table = table.substring(0, table.length - 2);
            resolve(table + '<br><br>' + JSON.stringify(response));*/
    }).catch(err => {
      var error = new Error(400, null, null, err);
      reject(JSON.stringify(error));
    });
  });
}

function vessels(query) {
  return new Promise((resolve, reject) => {
    api.vessels(query).then(response => {
      resolve(response);
      /*
            console.log("vessels", response);
            var table = "";
            for (var current = 0; current < response.items.length; current++) {
              table += response.items[current] + ", ";
            }
            table = table.substring(0, table.length - 2);
            resolve(table + '<br><br>' + JSON.stringify(response));*/
    }).catch(err => {
      var error = new Error(400, null, null, err);
      reject(JSON.stringify(error));
    });
  });
}

function locations(query) {
  return new Promise((resolve, reject) => {
    api.locations(query).then(response => {
      resolve(response);

      /* console.log("locations", response);
       var table = "";
       for (var current = 0; current < response.items.length; current++) {
         table += response.items[current].value.name + ", ";
       }
       table = table.substring(0, table.length - 2);
       resolve(table + '<br><br>' + JSON.stringify(response));*/
    }).catch(err => {
      var error = new Error(400, null, null, err);
      reject(JSON.stringify(error));
    });
  });
}

function ports(query) {
  return new Promise((resolve, reject) => {
    api.ports(query).then(response => {
      resolve(response);

      /*console.log("ports", response);
      var table = "";
      for (var current = 0; current < response.items.length; current++) {
        table += response.items[current].name + ", ";
      }
      table = table.substring(0, table.length - 2);
      resolve(table + '<br><br>' + JSON.stringify(response));*/
    }).catch(err => {
      var error = new Error(400, null, null, err);
      reject(JSON.stringify(error));
    });
  });
}

/**
 * Retrieves details about a search.
 */
module.exports.getSearch = (event, context, callback) => {
  var searchId = utils.getParam(event.params, "searchId", "string");
  dynamo.getSearchById(searchId).then(data => {
    callback(null, data);
  }).catch(err => {
    var error = new Error(500,
      "An unknown error occurred. See the details for more information.",
      errorCodes.serverError,
      err);
    callback(JSON.stringify(error));
  });
};