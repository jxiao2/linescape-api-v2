"use strict";

import AWS from "aws-sdk";
import https from "https";
import dynamo from "../dynamo";
import api from "../lshift-api";
import Promise from 'bluebird';
import utils from "../utils";
import Error from "../models/error.js";
import momentTimezone from "moment-timezone";
import _ from 'lodash';
import errorCodes from "../globals/errorCodes.js";

/**
 * Adds a schedule to the user's favorites.
 */
module.exports.addFavoriteSchedule = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  dynamo.addFavoriteSchedules(cognitoId, [utils.getParam(event.params, "scheduleId", "int")]).then(response => {
    callback(null, null);
  }).catch(err => {
    var error = new Error(400, null, null, err);
    callback(JSON.stringify(error));
  });
};

/**
 * Adds a port to the user's favorites.
 */
module.exports.addFavoritePort = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  dynamo.addFavoritePorts(cognitoId, [utils.getParam(event.params, "portId", "int")]).then(response => {
    callback(null, null);
  }).catch(err => {
    var error = new Error(400, null, null, err);
    callback(JSON.stringify(error));
  });
};

/**
 * Adds a vessel to the user's favorites.
 */
module.exports.addFavoriteVessel = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  dynamo.addFavoriteVessels(cognitoId, [utils.getParam(event.params, "vesselId", "int")]).then(response => {
    callback(null, null);
  }).catch(err => {
    var error = new Error(400, null, null, err);
    callback(JSON.stringify(error));
  });
};

/**
 * Adds a route to the user's favorites.
 */
module.exports.addFavoriteRoute = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  dynamo.addFavoriteRoutes(cognitoId, event.data).then(response => {
    callback(null, null);
  }).catch(err => {
    var error = new Error(400, null, null, err);
    callback(JSON.stringify(error));
  });
};

/**
 * Removes a route from the user's favorites.
 */
module.exports.removeFavoriteRoute = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  var originId = utils.getParam(event.params, "originId", "int");
  var destinationId = utils.getParam(event.params, "destinationId", "int");
  dynamo.removeFavoriteRoute(cognitoId, originId, destinationId).then(response => {
    callback(null, null);
  }).catch(err => {
    var error = new Error(400, null, null, err);
    callback(JSON.stringify(error));
  });
};

/**
 * Removes a schedule from the user's favorites.
 */
module.exports.removeFavoriteSchedule = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  dynamo.removeFavoriteSchedules(cognitoId, [utils.getParam(event.params, "scheduleId", "int")]).then(response => {
    callback(null, null);
  }).catch(err => {
    var error = new Error(400, null, null, err);
    callback(JSON.stringify(error));
  });
};

/**
 * Removes a vessel from the user's favorites.
 */
module.exports.removeFavoriteVessel = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  dynamo.removeFavoriteVessels(cognitoId, [utils.getParam(event.params, "vesselId", "int")]).then(response => {
    callback(null, null);
  }).catch(err => {
    var error = new Error(400, null, null, err);
    callback(JSON.stringify(error));
  });
};

/**
 * Removes a port from the user's favorites.
 */
module.exports.removeFavoritePort = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  dynamo.removeFavoritePorts(cognitoId, [utils.getParam(event.params, "portId", "int")]).then(response => {
    callback(null, null);
  }).catch(err => {
    callback(err);
  });
};

/**
 * Adds a carrier to the user's favorites.
 */
module.exports.addFavoriteCarrier = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  dynamo.addFavoriteCarriers(cognitoId, [utils.getParam(event.params, "carrierId", "int")]).then(response => {
    callback(null, null);
  }).catch(err => {
    var error = new Error(400, null, null, err);
    callback(JSON.stringify(error));
  });
};

/**
 * Removes a carrier from the user's favorites.
 */
module.exports.removeFavoriteCarrier = (event, context, callback) => {
  var cognitoId = utils.getCognitoId(event);
  dynamo.removeFavoriteCarriers(cognitoId, [utils.getParam(event.params, "carrierId", "int")]).then(response => {
    callback(null, null);
  }).catch(err => {
    var error = new Error(400, null, null, err);
    callback(JSON.stringify(error));
  });
};
