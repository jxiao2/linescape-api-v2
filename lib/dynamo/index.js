'use strict';
import AWS from "aws-sdk";
import Promise from 'bluebird';
import UserProfile from "../models/userProfile.js";
import uuid from "node-uuid";
import utils from "../utils";
import DeliverySettingsValidator from "../validators/deliverySettingsValidator.js";
import querystring from 'querystring';
import _ from 'lodash';
import accountTypes from "../globals/accountTypes.js";
import moment from "moment";

let docClient = new AWS.DynamoDB.DocumentClient();
let userProfilesTable = process.env.SERVERLESS_PROJECT +
  "-userProfiles-" +
  process.env.SERVERLESS_STAGE;
let emailSchedulesTable = process.env.SERVERLESS_PROJECT +
  "-emailSchedules-" +
  process.env.SERVERLESS_STAGE;
let errorLogsTable = process.env.SERVERLESS_PROJECT +
  "-errorLogs-" +
  process.env.SERVERLESS_STAGE;
let notesTable = process.env.SERVERLESS_PROJECT +
  "-notes-" +
  process.env.SERVERLESS_STAGE;
let searchesTable = process.env.SERVERLESS_PROJECT +
  "-searches-" +
  process.env.SERVERLESS_STAGE;
let accountTypesTable = process.env.SERVERLESS_PROJECT +
  "-accountTypes-" +
  process.env.SERVERLESS_STAGE;

let SEARCH_LIMIT = 5;
let MS_PER_MINUTE = 60000;

let UNREGISTERED_ACCOUNT_TYPE = {
  id: accountTypes.unregistered,
  limits: {
    maxSchedules: 0,
    maxSearchesPerInterval: 100,
    searchResetIntervalMinutes: 60
  }
};

module.exports.setUserPersonalInformation = (userIdentityId, personalInformation) => {
  console.log("in dynamo setUserPersonalInformation", personalInformation);

  return new Promise((resolve, reject) => {
    var updateParams = {
      TableName: userProfilesTable,
      Key: {
        cognitoId: userIdentityId
      },
      ExpressionAttributeValues: {
        ":personalInformation": personalInformation
      },
      ExpressionAttributeNames: {
        "#personalInformation": "personalInformation"
      },
      UpdateExpression: "set #personalInformation = :personalInformation"
    };

    docClient.update(updateParams, (err, data) => {
      if (err) {
        console.log("client error", err);
        return reject(err);
      }
      else {
        return resolve(data);
      }
    });
  });
}

/**
 * Updates the user's profile after registration. Sets the email and registerDate fields.
 * @param {string} userIdentityId - Th`e cognito identity id for the user.
 * @param {string} email - The user's email.
 */
module.exports.makeUserRegistered = (userIdentityId, email) => {
  console.log("in make user registered. userIdentityId: ", userIdentityId);
  console.log("in make user registered. email: ", email);

  return new Promise((resolve, reject) => {
    module.exports.getUserFromEmail(email).then(user => {
      console.log("in GET USER FROM EMAIL callback", email);
      if (user.cognitoId === userIdentityId) {
        console.log("cognito id is equal, not deleting", userIdentityId);
        return resolve(user);
      }
      console.log("in checkIfUser then (found existing user with id: ", user.cognitoId);
      // Imported user. Need to updated identity id.
      var registrationDate = utils.UTCDate().toString();
      var updateParams = {};

      if (user.account.confirmed === false) {
        var plan = user.plan;
        user.account.confirmed = true;
        
        if (_.isNil(plan)) {
          plan = {
              "id": null,
              "name": null,
              "amount": null,
              "interval": null,
              "trialPeriodDays": null
            };
        }

        var personalInformation = {};
        try {
          personalInformation = user.personalInformation;
        } catch (ex) {
          personalInformation = {
            firstName: '',
            lastName: ''
          };
        }

        updateParams = {
          TableName: userProfilesTable,
          Key: {
            cognitoId: userIdentityId
          },
          ExpressionAttributeValues: {
            ":account": user.account,
            ":plan": plan,
            ":registrationDate": registrationDate,
            ":email": email,
            ":personalInformation": personalInformation,
            ":favorites": {}
          },
          ExpressionAttributeNames: {
            "#account": "account",
            "#plan": "plan",
            "#email": "email",
            "#registrationDate": "registerDate",
            "#favorites": "favorites",
            "#personalInformation": "personalInformation"
          },
          UpdateExpression: "set #account = :account, #plan = :plan, #email = :email, #registrationDate = :registrationDate, #favorites = :favorites, #personalInformation = :personalInformation"
        };
      }
      else {
        updateParams = {
          TableName: userProfilesTable,
          Key: {
            cognitoId: userIdentityId
          },
          ExpressionAttributeValues: {
            ":registrationDate": registrationDate,
            ":email": email,
            ":account": {
              "type": accountTypes.basic,
              "stripeId": null
            },
            ":plan": {
              "id": null,
              "name": null,
              "amount": null,
              "interval": null,
              "trialPeriodDays": null
            },
            ":favorites": {}
          },
          ExpressionAttributeNames: {
            "#email": "email",
            "#registrationDate": "registerDate",
            "#account": "account",
            "#plan": "plan",
            "#favorites": "favorites"
          },
          UpdateExpression: "set #email = :email, #registrationDate = :registrationDate, #account = :account, #plan = :plan, #favorites = :favorites"
        };
      }

      console.log("about to update with duplicate dynamo");
      docClient.update(updateParams, (err, data) => {
        if (err) {
          console.log("client error", err);
          return reject(err);
        }
        else {
          console.log("successfully added new user");

          var deleteParams = {
            TableName: userProfilesTable,
            Key: {
              "cognitoId": user.cognitoId
            }
          };
          console.log("about to delete");
          docClient.delete(deleteParams, function(err, deleteData) {
            if (err) {
              console.error("Unable to delete item. Error JSON:", JSON.stringify(err, null, 2));
              return reject(err);
            }
            else {
              console.log("DeleteItem succeeded:", JSON.stringify(deleteData, null, 2));
              return resolve(user);
            }
          });
        }
      });

    }).catch(err => {
      // Standard case -- newly registered user
      var registrationDate = utils.UTCDate().toString();

      var params = {
        TableName: userProfilesTable,
        Key: {
          cognitoId: userIdentityId
        },
        ExpressionAttributeValues: {
          ":registrationDate": registrationDate,
          ":email": email,
          ":account": {
            "type": accountTypes.basic,
            "stripeId": null
          },
          ":plan": {
            "id": null,
            "name": null,
            "amount": null,
            "interval": null,
            "trialPeriodDays": null
          },
          ":favorites": {}
        },
        ExpressionAttributeNames: {
          "#email": "email",
          "#registrationDate": "registerDate",
          "#account": "account",
          "#plan": "plan",
          "#favorites": "favorites"
        },
        UpdateExpression: "set #email = :email, #registrationDate = :registrationDate, #account = :account, #plan = :plan, #favorites = :favorites"
      };

      docClient.update(params, (err, data) => {
        if (err) {
          console.log("client error", err);
          return reject(err);
        }
        else {
          return resolve(data);
        }
      });
    });

  });
};

module.exports.makeCustomerRegistered = (userIdentityId, email, token, coupon, planId, firstName, lastName) => {
  console.log('makeCustomerRegistered', userIdentityId, email, token, coupon, planId, firstName, lastName);
  return new Promise((resolve, reject) => {
    var registrationDate = utils.UTCDate().toString();
    console.log(1);
    var accountParams = {
      confirmed: false,
    };

    if (planId !== '' && planId !== null) {
      accountParams.planId = planId;
    }
    console.log(2);
    if (token !== '' && token !== null && token !== undefined) {
      console.log('token');
      accountParams.token = token;
    }
    console.log(3);
    if (coupon !== '' && coupon !== null && coupon !== undefined) {
      console.log('coupon');
      accountParams.coupon = coupon;
    }
    console.log('account', accountParams);

    var personalInformationParams = {
      firstName: firstName,
      lastName: lastName
    };

    var params = {
      TableName: userProfilesTable,
      Key: {
        cognitoId: userIdentityId
      },
      ExpressionAttributeValues: {
        ":registrationDate": registrationDate,
        ":email": email,
        ":account": accountParams,
        ":personalInformation": personalInformationParams
      },
      ExpressionAttributeNames: {
        "#email": "email",
        "#registrationDate": "registerDate",
        "#account": "account",
        "#personalInformation": "personalInformation"
      },
      UpdateExpression: "set #email = :email, #registrationDate = :registrationDate, #account = :account, #personalInformation = :personalInformation"
    };
    console.log('params', params);
    docClient.update(params, (err, data) => {
      console.log('makeCustomerRegistered docClient update', err, data);
      if (err) {
        console.log("client error", err);
        return reject(err);
      }
      else {
        return resolve(data);
      }
    });
  });
};

module.exports.getUserAccountType = (userIdentityId) => {
  return new Promise((resolve, reject) => {
    module.exports.getUserProfile(userIdentityId).then(result => {
      if (_.isNil(result) || _.isNil(result.account) || result.account.type === undefined) {
        return resolve(UNREGISTERED_ACCOUNT_TYPE);
      }

      var params = {
        TableName: accountTypesTable,
        Key: {
          id: result.account.type
        }
      };

      docClient.get(params, function(err, data) {
        if (err) {
          reject(err);
        }
        resolve(data != null ? data.Item : null);
      });
    });

  });
};

/**
 * Creates a blank userProfile for the user if one doesn't already exist.
 * @param {string} userIdentityId - The cognito identity id for the user.
 */
module.exports.createUserProfileIfNotExists = (userIdentityId) => {
  console.log("attempting to create user profile");
  return new Promise((resolve, reject) => {
    var params = {
      TableName: userProfilesTable,
      Key: {
        cognitoId: userIdentityId
      },
      ConditionExpression: 'attribute_not_exists(cognitoId)',
      Item: new UserProfile(userIdentityId)
    };

    docClient.put(params, (err, data) => {
      console.log(JSON.stringify(err));
      if (err && err.code !== "ConditionalCheckFailedException") {
        return reject(err);
      }
      else {
        return resolve(data);
      }
    });
  });
};

/**
 * Adds the specified ports to the user's favorites.
 * @param {string} userIdentityId - The AWS Cognito Identity id of the user.
 * @param {number[]} portIds - The data API ids of the ports to add.
 */
module.exports.addFavoritePorts = (userIdentityId, portIds) => {
  return new Promise((resolve, reject) => {
    if (typeof portIds === 'undefined' || portIds.length === 0) {
      return reject("Please specify at least one portId");
    }

    module.exports.createUserProfileIfNotExists(userIdentityId).then(() => {
      var params = {
        TableName: userProfilesTable,
        Key: {
          cognitoId: userIdentityId
        },
        UpdateExpression: 'ADD #favorites.ports :portIds',
        ExpressionAttributeValues: {
          ":portIds": docClient.createSet(portIds)
        },
        ExpressionAttributeNames: {
          "#favorites": "favorites"
        }
      };

      docClient.update(params, (err, data) => {
        if (err) {
          return reject(err);
        }
        resolve(data);
      });
    });
  });
};

/**
 * Adds the specified routes to the user's favorites.
 * @param {string} userIdentityId - The AWS Cognito Identity id of the user.
 * @param {number[]} routeIds - The data API ids of the routes to add.
 */
module.exports.addFavoriteRoutes = (userIdentityId, data) => {
  return new Promise((resolve, reject) => {
    if (typeof data.origin_id === 'undefined' || typeof data.destination_id === 'undefined') {
      return reject("Please specify the origin and destination port ids");
    }
    let routeToSave = {
      originId: data.origin_id,
      originName: data.origin_name,
      destinationId: data.destination_id,
      destinationName: data.destination_name,
    };

    module.exports.createUserProfileIfNotExists(userIdentityId).then(() => {
      var params = {
        TableName: userProfilesTable,
        Key: {
          cognitoId: userIdentityId
        },
        UpdateExpression: 'SET #favorites.routes = list_append(if_not_exists(#favorites.routes, :emptyArray), :route)',
        ExpressionAttributeValues: {
          ":route": [routeToSave],
          ":emptyArray": []
        },
        ExpressionAttributeNames: {
          "#favorites": "favorites"
        },
        ReturnValues: "UPDATED_NEW"
      };

      docClient.update(params, (err, data) => {
        if (err) {
          console.log(JSON.stringify(err));
          return reject(err);
        }
        resolve(data.Attributes);
      });
    });
  });
};

/**
 * Adds the specified schedules to the user's favorites.
 * @param {string} userIdentityId - The AWS Cognito Identity id of the user.
 * @param {number[]} scheduleIds - The data API ids of the schedules to add.
 */
module.exports.addFavoriteSchedules = (userIdentityId, scheduleIds) => {
  return new Promise((resolve, reject) => {
    if (typeof scheduleIds === 'undefined' || scheduleIds.length === 0) {
      return reject("Please specify at least one scheduleId");
    }

    module.exports.createUserProfileIfNotExists(userIdentityId).then(() => {
      var params = {
        TableName: userProfilesTable,
        Key: {
          cognitoId: userIdentityId
        },
        UpdateExpression: 'ADD #favorites.schedules :scheduleIds',
        ExpressionAttributeValues: {
          ":scheduleIds": docClient.createSet(scheduleIds)
        },
        ExpressionAttributeNames: {
          "#favorites": "favorites"
        }
      };

      docClient.update(params, (err, data) => {
        if (err) {
          return reject(err);
        }
        resolve(data);
      });
    });
  });
};

/**
 * Records a user's search and increments the number of searches toward their limit.
 */
module.exports.incrementUserSearchesOrRejectOnLimitExceeded = (userIdentityId) => {
  return new Promise((resolve, reject) => {

    module.exports.getUserAccountType(userIdentityId).then(accountType => {
      if (!accountType) {
        accountType = UNREGISTERED_ACCOUNT_TYPE;
      }
      var interval = accountType.limits.searchResetIntervalMinutes;
      var limit = accountType.limits.maxSearchesPerInterval;
      var date = utils.UTCDate();
      var cutoff = date - (interval * MS_PER_MINUTE);

      module.exports.numberOfSearchesSinceDate(userIdentityId, new Date(cutoff)).then(count => {
        if (count > limit) {
          return reject(`Sorry, you have exceeded your search limit. Please try again later. To get more searches you may also upgrade to a Premium or Professional account.`);
        }
        return resolve(accountType);
      });
    });

  });
};

/**
 * Retrieves a search by id.
 */
module.exports.getSearchById = (searchId) => {
  return new Promise((resolve, reject) => {
    console.log("searchId", searchId);
    var params = {
      TableName: searchesTable,
      KeyConditionExpression: "id = :searchId",
      ExpressionAttributeValues: {
        ':searchId': searchId
      }

    };

    docClient.query(params, function(err, data) {
      if (err) {
        return reject(err);
      }
      if (data.Count === 0) {
        return resolve(null);
      }
      resolve(data.Items[0]);
    });
  });
};

/**
 * Stores a search in the searches table.
 */
module.exports.recordSearch = (userIdentityId, details, searchType, path) => {
  return new Promise((resolve, reject) => {
    var id = uuid.v4();
    var params = {
      TableName: searchesTable,
      Key: {
        id: id,
        msSinceEpoch: utils.UTCDate().getTime()
      },
      ExpressionAttributeValues: {
        ":cognitoId": userIdentityId,
        ":details": details,
        ":queryString": querystring.stringify(details),
        ":searchType": searchType,
        ":relativePath": path
      },
      UpdateExpression: 'set details = :details, cognitoId = :cognitoId, queryString = :queryString, searchType = :searchType, relativePath = :relativePath'
    };

    docClient.update(params, (err, data) => {
      if (err) {
        console.log("search error", JSON.stringify(err));
      }
      return resolve(id);
    });
  });
};

/**
 * Gets the number of searches a user has made since a given datetime.
 */
module.exports.numberOfSearchesSinceDate = (userIdentityId, date) => {
  return new Promise((resolve, reject) => {
    var ticks = date.getTime();

    var params = {
      TableName: searchesTable,
      IndexName: "cognitoId",
      KeyConditionExpression: "cognitoId = :cognitoId AND msSinceEpoch >= :ms",
      ExpressionAttributeValues: {
        ":ms": ticks,
        ":cognitoId": userIdentityId
      },
      ScanIndexForward: false
    };

    docClient.query(params, (err, data) => {
      if (err) {
        return reject(err);
      }
      else {
        console.log(`found ${data.Count} items!`);
        return resolve(data.Count);
      }
    });
  });
};

/**
 * Adds the specified vessels to the user's favorites.
 * @param {string} userIdentityId - The AWS Cognito Identity id of the user.
 * @param {number[]} vesselIds - The data API ids of the vessels to add.
 */
module.exports.addFavoriteVessels = (userIdentityId, vesselIds) => {
  return new Promise((resolve, reject) => {
    if (typeof vesselIds === 'undefined' || vesselIds.length === 0) {
      return reject("Please specify at least one vesselId");
    }
    module.exports.createUserProfileIfNotExists(userIdentityId).then(() => {
      var params = {
        TableName: userProfilesTable,
        Key: {
          cognitoId: userIdentityId
        },
        UpdateExpression: 'ADD #favorites.vessels :vesselIds',
        ExpressionAttributeValues: {
          ":vesselIds": docClient.createSet(vesselIds)
        },
        ExpressionAttributeNames: {
          "#favorites": "favorites"
        }
      };

      docClient.update(params, (err, data) => {
        if (err) {
          return reject(err);
        }
        resolve(data);
      });
    });
  });
};

/**
 * Adds the specified carriers to the user's favorites.
 * @param {string} userIdentityId - The AWS Cognito Identity id of the user.
 * @param {number[]} carrierIds - The data API ids of the carriers to add.
 */
module.exports.addFavoriteCarriers = (userIdentityId, carrierIds) => {
  return new Promise((resolve, reject) => {
    if (typeof carrierIds === 'undefined' || carrierIds.length === 0) {
      return reject("Please specify at least one carrierId");
    }

    module.exports.createUserProfileIfNotExists(userIdentityId).then(() => {
      var params = {
        TableName: userProfilesTable,
        Key: {
          cognitoId: userIdentityId
        },
        UpdateExpression: 'ADD #favorites.carriers :carrierIds',
        ExpressionAttributeValues: {
          ":carrierIds": docClient.createSet(carrierIds)
        },
        ExpressionAttributeNames: {
          "#favorites": "favorites"
        }
      };

      docClient.update(params, (err, data) => {
        if (err) {
          return reject(err);
        }
        resolve(data);
      });
    });
  });
};

/**
 * Removes the specified carriers from the user's favorites.
 * @param {string} userIdentityId - The AWS Cognito Identity id of the user.
 * @param {number[]} carrierIds - The data API ids of the carriers to remove.
 */
module.exports.removeFavoriteCarriers = (userIdentityId, carrierIds) => {
  return new Promise((resolve, reject) => {
    var params = {
      TableName: userProfilesTable,
      Key: {
        cognitoId: userIdentityId
      },
      UpdateExpression: 'DELETE #favorites.carriers :carrierIds',
      ExpressionAttributeValues: {
        ":carrierIds": docClient.createSet(carrierIds)
      },
      ExpressionAttributeNames: {
        "#favorites": "favorites"
      }
    };

    docClient.update(params, (err, data) => {
      if (err) {
        return reject(err);
      }
      resolve(data);
    });
  });
};

/**
 * Removes the specified schedules from the user's favorites.
 * @param {string} userIdentityId - The AWS Cognito Identity id of the user.
 * @param {number[]} scheduleIds - The data API ids of the schedules to remove.
 */
module.exports.removeFavoriteSchedules = (userIdentityId, scheduleIds) => {
  return new Promise((resolve, reject) => {
    var params = {
      TableName: userProfilesTable,
      Key: {
        cognitoId: userIdentityId
      },
      UpdateExpression: 'DELETE #favorites.schedules :scheduleIds',
      ExpressionAttributeValues: {
        ":scheduleIds": docClient.createSet(scheduleIds)
      },
      ExpressionAttributeNames: {
        "#favorites": "favorites"
      }
    };

    docClient.update(params, (err, data) => {
      if (err) {
        return reject(err);
      }
      resolve(data);
    });
  });
};


/**
 * Removes the specified route from the user's favorites.
 * @param {string} userIdentityId - The AWS Cognito Identity id of the user.
 * @param {number} originId - The data API id of the origin port to remove.
 * @param {number} destinationId - The data API id of the destination port to remove.
 */
module.exports.removeFavoriteRoute = (userIdentityId, originId, destinationId) => {
  return new Promise((resolve, reject) => {
    module.exports.getUserFavorites(userIdentityId).then((result) => {
      var existingRoutes = result.routes;
      var newRoutesArr = _.filter(existingRoutes, function (o) {
        return o.originId !== originId && o.destinationId !== destinationId;
      });

      var params = {
        TableName: userProfilesTable,
        Key: {
          cognitoId: userIdentityId
        },
        UpdateExpression: 'SET #favorites.routes :routeIds',
        ExpressionAttributeValues: {
          ":routeIds": newRoutesArr
        },
        ExpressionAttributeNames: {
          "#favorites": "favorites"
        }
      };
      if (newRoutesArr.length === 0) {
        params.UpdateExpression = 'REMOVE #favorites.routes';
        _.unset(params, "ExpressionAttributeValues");
      }

      docClient.update(params, (err, data) => {
        if (err) {
          console.log(JSON.stringify(err));
          return reject(err);
        }
        resolve(data);
      });
    });
  });
};

/**
 * Removes the specified vessels from the user's favorites.
 * @param {string} userIdentityId - The AWS Cognito Identity id of the user.
 * @param {number[]} vesselIds - The data API ids of the vessels to remove.
 */
module.exports.removeFavoriteVessels = (userIdentityId, vesselIds) => {
  return new Promise((resolve, reject) => {
    var params = {
      TableName: userProfilesTable,
      Key: {
        cognitoId: userIdentityId
      },
      UpdateExpression: 'DELETE #favorites.vessels :vesselIds',
      ExpressionAttributeValues: {
        ":vesselIds": docClient.createSet(vesselIds)
      },
      ExpressionAttributeNames: {
        "#favorites": "favorites"
      }
    };

    docClient.update(params, (err, data) => {
      if (err) {
        return reject(err);
      }
      resolve(data);
    });
  });
};

/**
 * Removes the specified vessels from the user's favorites.
 * @param {string} userIdentityId - The AWS Cognito Identity id of the user.
 * @param {number[]} portIds - The data API ids of the ports to remove.
 */
module.exports.removeFavoritePorts = (userIdentityId, portIds) => {
  return new Promise((resolve, reject) => {
    var params = {
      TableName: userProfilesTable,
      Key: {
        cognitoId: userIdentityId
      },
      UpdateExpression: 'DELETE #favorites.ports :portIds',
      ExpressionAttributeValues: {
        ":portIds": docClient.createSet(portIds)
      },
      ExpressionAttributeNames: {
        "#favorites": "favorites"
      }
    };

    console.log(params);

    docClient.update(params, (err, data) => {
      if (err) {
        return reject(err);
      }
      resolve(data);
    });
  });
};

/**
 * Retrieves the specified user's profile.
 * @param {string} userIdentityId - The AWS Cognito Identity id of the user.
 */
module.exports.getUserProfile = (userIdentityId) => {
  return new Promise((resolve, reject) => {
    console.log(userIdentityId);
    if (!userIdentityId) {
      return reject("An identity id must be specified");
    }
    var params = {
      TableName: userProfilesTable,
      Key: {
        cognitoId: userIdentityId
      }
    };

    docClient.get(params, function(err, data) {
      if (err) {
        reject(err);
      }
      resolve(data != null ? data.Item : null);
    });
  });
};

/**
 * Returns true/false if user has an account
 * @param {string} userIdentityId - The AWS Cognito Identity id of the user.
 */
module.exports.checkIfUser = (userEmail) => {
  return new Promise((resolve, reject) => {
    console.log(userEmail);
    if (!userEmail) {
      return reject("An email address must be specified");
    }
    var params = {
      TableName: userProfilesTable,
      IndexName: "email",
      KeyConditionExpression: "email = :userEmail",
      ExpressionAttributeValues: {
        ':userEmail': userEmail
      },
      ScanIndexForward: false
    };

    docClient.query(params, function(err, data) {
      console.log('checkIfUser data', JSON.stringify(data));
      if (err) {
        reject(err);
      }
      if (data.Count === 0) {
        reject("User does not have an account");
      }
      // user is in database (not necessarily confirmed yet)
      try {
        if (data.Items[0].account.confirmed === false && data.Items[0].account.type === undefined) {
          reject("User account is not confirmed yet");
        }

        resolve(data.Items[0]);
      }
      catch (ex) {
        resolve(data.Items[0]);
      }
    });
  });
};

/**
 * Returns true/false if user has an account
 * @param {string} userIdentityId - The AWS Cognito Identity id of the user.
 */
module.exports.getUserFromEmail = (userEmail) => {
  return new Promise((resolve, reject) => {
    console.log(userEmail);
    if (!userEmail) {
      return reject("An email address must be specified");
    }
    var params = {
      TableName: userProfilesTable,
      IndexName: "email",
      KeyConditionExpression: "email = :userEmail",
      ExpressionAttributeValues: {
        ':userEmail': userEmail
      },
      ScanIndexForward: false
    };

    docClient.query(params, function(err, data) {
      console.log('getuserfromemail data', JSON.stringify(data));
      if (err) {
        reject(err);
      }
      if (data.Count === 0) {
        resolve(null);
      }
        resolve(data.Items[0]);
    });
  });
};

/**
 * Retrieves the favorites for the specified user.
 * @param {string} userIdentityId - The AWS Cognito Identity id of the user.
 */
module.exports.getUserFavorites = (userIdentityId) => {
  return new Promise((resolve, reject) => {
    if (!userIdentityId) {
      return resolve(null);
    }
    var params = {
      TableName: userProfilesTable,
      Key: {
        cognitoId: userIdentityId
      },
      AttributesToGet: [
        "favorites"
      ]
    };

    docClient.get(params, function(err, data) {
      if (err) {
        reject(err);
      }
      resolve(data != null ? data.Item : {});
    });
  });
};

/**
 * Adds a note to the specified schedule.
 */
module.exports.addScheduleNote = (userIdentityId, scheduleId, note) => {
  return new Promise((resolve, reject) => {
    var params = {
      TableName: notesTable,
      Key: {
        cognitoId: userIdentityId,
        itemId: scheduleId
      },
      ExpressionAttributeValues: {
        ":note": note,
        ":itemType": "schedule"
      },
      UpdateExpression: "set note = :note, itemType = :itemType"
    };

    console.log(JSON.stringify(params));

    docClient.update(params, (err, data) => {
      if (err) {
        console.log("client error", err);
        return reject(err);
      }
      else {
        return resolve(data);
      }
    });
  });
};

/**
 * Retrieves a note for the specified schedule
 */
module.exports.getScheduleNote = (userIdentityId, scheduleId) => {
  return new Promise((resolve, reject) => {
    if (!userIdentityId) {
      return reject("An identity id must be specified");
    }
    var params = {
      TableName: notesTable,
      KeyConditionExpression: "cognitoId = :cognitoId AND itemId = :itemId",
      ExpressionAttributeValues: {
        ":itemType": "schedule",
        ":itemId": scheduleId,
        ":cognitoId": userIdentityId
      },
      FilterExpression: "itemType = :itemType"
    };

    docClient.query(params, function(err, data) {
      if (err) {
        return reject(err);
      }
      resolve(data);
    });
  });
};

/**
 * Adds an error to the error log.
 * @param {object} data - A JSON object with the error information
 */
module.exports.addErrorLog = (data) => {
  return new Promise((resolve, reject) => {
    var params = {
      TableName: errorLogsTable,
      Key: {
        id: uuid.v4(),
        msSinceEpoch: new Date().getTime()
      },
      ExpressionAttributeValues: {
        ":data": data
      },
      UpdateExpression: "set errorData = :data"
    };

    docClient.update(params, (err, data) => {
      if (err) {
        return reject(err);
      }
      else {
        return resolve(data);
      }
    });
  });
};

/**
 * Retrieves client error logs from the database.
 */
module.exports.getErrorLogs = () => {
  return new Promise((resolve, reject) => {
    var params = {
      TableName: errorLogsTable,
    };

    docClient.scan(params, (err, data) => {
      if (err) {
        return reject(err);
      }
      else {
        return resolve(data);
      }
    });
  });
};

/**
 * Updates or creates the user's delivery settings.
 */
module.exports.updateDeliverySettings = (cognitoId, settings) => {
  return new Promise((resolve, reject) => {

    if (typeof settings.userTimeZone !== 'undefined') {
      settings.userTimeZone = settings.userTimeZone.replace(/ /g, '_');
    }

    module.exports.createUserProfileIfNotExists(cognitoId).then(() => {
      // not sure if this solution deals with daylight savings

      let utc = moment.utc();
      let offset = moment.tz.zone(settings.userTimeZone).offset(utc); // returns in minutes
      offset = (offset / 60);
      console.log('offset in hours:', offset);
      let daysOfWeek = [];
      let hourOfDay = settings.hourOfDay + offset;
      let dayCorrection = 0;
      if (settings.hourOfDay + offset < 0) {
        dayCorrection = -1;
        hourOfDay = 24 + hourOfDay;
      }
      if (settings.hourOfDay + offset > 24) {
        dayCorrection = 1;
        hourOfDay = hourOfDay - 24;
      }

      for (let current = 0; current < settings.daysOfWeek.length; current++) {
        let day = settings.daysOfWeek[current] + dayCorrection;
        daysOfWeek.push(day);
      }

      var params = {
        TableName: emailSchedulesTable,
        Key: {
          cognitoId: cognitoId
        },
        UpdateExpression: 'SET hourOfDay = :hourOfDay, daysOfWeek = :daysOfWeek, emailSelf = :emailSelf, htmlEmails = :htmlEmails, userTimeZone = :timeZone, otherEmails = :otherEmails',
        ExpressionAttributeValues: {
          ":hourOfDay": hourOfDay,
          ":daysOfWeek": daysOfWeek || [],
          ":emailSelf": settings.emailSelf,
          ":htmlEmails": settings.htmlEmails,
          ":timeZone": settings.userTimeZone,
          ":otherEmails": settings.otherEmails || []
        }
      };

      docClient.update(params, (err, data) => {
        if (err) {
          return reject(err);
        }
        resolve(data);
      });
    });
  });
};


/**
 * Retrieves the specified user's delivery settings.
 * @param {string} userIdentityId - The AWS Cognito Identity id of the user.
 */
module.exports.getDeliverySettings = (userIdentityId) => {
  return new Promise((resolve, reject) => {
    console.log(userIdentityId);
    if (!userIdentityId) {
      return reject("An identity id must be specified");
    }

    module.exports.createUserProfileIfNotExists(userIdentityId).then(() => {
      var params = {
        TableName: emailSchedulesTable,
        Key: {
          cognitoId: userIdentityId
        }
      };

      docClient.get(params, function(err, data) {
        console.log('docClient get', err, data);
        if (err) {
          reject(err);
        }
        if (data) {
          try {
            let item = data.Item;

            let utc = moment.utc();
            let offset = moment.tz.zone(item.userTimeZone).offset(utc); // returns in minutes
            offset = (offset / 60);
            console.log('offset in hours:', offset);
            let daysOfWeek = [];
            let hourOfDay = item.hourOfDay - offset;
            let dayCorrection = 0;
            if (item.hourOfDay - offset < 0) {
              dayCorrection = -1;
              hourOfDay = 24 + hourOfDay;
            }
            if (item.hourOfDay - offset > 24) {
              dayCorrection = 1;
              hourOfDay = hourOfDay - 24;
            }

            for (let current = 0; current < item.daysOfWeek.length; current++) {
              let day = item.daysOfWeek[current] + dayCorrection;
              daysOfWeek.push(day);
            }

            item.hourOfDay = hourOfDay;
            item.daysOfWeek = daysOfWeek;

            resolve(item);
          }
          catch (ex) {
            resolve(null); // settings do not exist
          }
        }
        resolve(null);
      });
    });
  });
};

/**
 * Adds an email schedule to the specified user if they haven't exceeded the limit.
 *
 */
module.exports.addEmailSchedule = (userIdentityId, data) => {
  return new Promise((resolve, reject) => {
    data.id = uuid.v4();

    module.exports.getEmailSchedules(userIdentityId).then((result) => {
      var existingEmailSchedules = result;
      var newScheduleArr;
      if (existingEmailSchedules !== undefined) {
        newScheduleArr = _.filter(existingEmailSchedules, function(o) {
          return o.id !== data.scheduleId;
        });
        newScheduleArr.push(data);
      }
      else {
        newScheduleArr = [data];
      }

      var params = {
        TableName: emailSchedulesTable,
        Key: {
          cognitoId: userIdentityId
        },
        UpdateExpression: 'SET #emailSchedules = :schedule',
        ExpressionAttributeValues: {
          ":schedule": newScheduleArr
        },
        ExpressionAttributeNames: {
          "#emailSchedules": "emailSchedules"
        },
        ReturnValues: "UPDATED_NEW"
      };

      docClient.update(params, (err, data) => {
        if (err) {
          console.log(JSON.stringify(err));
          return reject(err);
        }
        resolve(data.Attributes);
      });
    });
  });
};

module.exports.checkIfEmailScheduleCanBeAddedOrUpdated = (userIdentityId, scheduleId, limit) => {
  return new Promise((resolve, reject) => {
    console.log(userIdentityId);
    if (!userIdentityId) {
      return reject("An identity id must be specified");
    }

    var params = {
      TableName: emailSchedulesTable,
      Key: {
        cognitoId: userIdentityId
      }
    };

    docClient.get(params, function(err, data) {
      if (err) {
        return reject(err);
      }

      module.exports.getEmailSchedules(userIdentityId).then(result => {
        var count = 0;
        if (typeof result !== 'undefined' && result !== null) {
          count = result.length;
        }

        if (count < limit) {
          resolve(true);
        }

        if (typeof result !== 'undefined' && result !== null) {
          for (var i = 0; i < result.length; i++) {
            if (result[i].id === scheduleId && count <= limit) {
              resolve(true);
            }
          }
        }

        resolve(false);
      });
    });
  });
};

module.exports.rejectOnEmailScheduleCountExceeded = (userIdentityId, scheduleId) => {
  return new Promise((resolve, reject) => {

    module.exports.getUserAccountType(userIdentityId).then(accountType => {
      var limit = accountType.limits.maxSchedules;

      module.exports.checkIfEmailScheduleCanBeAddedOrUpdated(userIdentityId, scheduleId, limit).then(canBeAddedOrUpdated => {
        if (!canBeAddedOrUpdated) {

          return reject(`Sorry, you can only have ${limit} email schedules under your current plan.`);
        }
        return resolve();
      });
    });
  });
};

/**
 * Removes an email schedule from the specified user.
 */
module.exports.removeEmailSchedule = (userIdentityId, scheduleId) => {
  return new Promise((resolve, reject) => {
    module.exports.getEmailSchedules(userIdentityId).then((result) => {

      var existingSchedules = result;
      var newScheduleArr = _.filter(existingSchedules, function(o) {
        return o.id !== scheduleId;
      });

      var params = {
        TableName: emailSchedulesTable,
        Key: {
          cognitoId: userIdentityId
        },
        UpdateExpression: 'SET #emailSchedules = :schedule',
        ExpressionAttributeValues: {
          ":schedule": newScheduleArr
        },
        ExpressionAttributeNames: {
          "#emailSchedules": "emailSchedules"
        }
      };

      if (newScheduleArr.length === 0) {
        params.UpdateExpression = 'REMOVE #emailSchedules';
        _.unset(params, "ExpressionAttributeValues");
      }

      docClient.update(params, (err, data) => {
        if (err) {
          console.log(JSON.stringify(err));
          return reject(err);
        }
        resolve(data);
      });
    });
  });
};

/**
 * Retrieves the specified user's email schedules.
 * @param {string} userIdentityId - The AWS Cognito Identity id of the user.
 */
module.exports.getEmailSchedules = (userIdentityId) => {
  return new Promise((resolve, reject) => {

    module.exports.createUserProfileIfNotExists(userIdentityId).then(() => {
      console.log(userIdentityId);
      if (!userIdentityId) {
        return reject("An identity id must be specified");
      }
      console.log('getEmailSchedules before params');
      var params = {
        TableName: emailSchedulesTable,
        Key: {
          cognitoId: userIdentityId
        }
      };
      docClient.get(params, function(err, data) {
        if (err) {
          reject(err);
        }
        console.log('getEmailSchedules data', data);
        try {
          resolve(data !== null ? data.Item.emailSchedules : null);
        }
        catch (ex) {
          resolve(null);
        }
      });
    });
  });
};

/**
 * Adds the specified stripe id to the user.
 * @param {string} userIdentityId - The AWS Cognito Identity id of the user.
 * @param {string} stripeId - The stripe id to associate to the user..
 */
module.exports.addStripeId = (userIdentityId, stripeId) => {
  return new Promise((resolve, reject) => {
    var params = {
      TableName: userProfilesTable,
      Key: {
        cognitoId: userIdentityId
      },
      ExpressionAttributeValues: {
        ":stripeId": stripeId
      },
      ExpressionAttributeNames: {
        "#account": "account"
      },
      UpdateExpression: "set #account.stripeId = :stripeId"
    };

    docClient.update(params, (err, data) => {
      if (err) {
        return reject(err);
      }
      else {
        return resolve(data);
      }
    });
  });
};

/**
 * Retrieves the stripeId of the user.
 * @param {string} userIdentityId - The AWS Cognito Identity id of the user.
 */
module.exports.getUserStripeId = (userIdentityId) => {
  return new Promise((resolve, reject) => {
    if (!userIdentityId) {
      return reject("An identity id must be specified");
    }
    var params = {
      TableName: userProfilesTable,
      Key: {
        cognitoId: userIdentityId
      },
      AttributesToGet: [
        "account"
      ]
    };

    docClient.get(params, function(err, data) {
      if (err) {
        reject(err);
      }
      if (data === null) {
        reject("no stripeId associated with the user.");
      } // review and make standard
      resolve(data.Item.account.stripeId);
    });
  });
};

/**
 * Updates or creates the user's plan.
 */
module.exports.updateUserPlan = (userIdentityId, stripeId, plan) => {
  // use plan name instead of ID because ID cannot be repeated
  // first word is the period (Monthly, Yearly)
  // space
  // second word is the plan id (Premium, Professional)
  //ex: monthly-professional, yearly-premium

  // for api plans
  // first word is api
  // second is amount 
  // ex: api-1000, api-4000
  return new Promise((resolve, reject) => {
    console.log('updateUserPlan', plan)
    var accountType = 'Basic';
    if (plan.name !== null) {
      try {
        let planArray = plan.id.split('-');
        if (planArray[1].toLowerCase() === 'professional') {
          accountType = 'Professional';
        }
        else if (planArray[1].toLowerCase() === 'premium') {
          accountType = 'Premium';
        }
        else if (planArray[0].toLowerCase() == 'api') {
          accountType = 'Professional';
        }

        console.log('updateUserPlan accountType', accountType);
      }
      catch (ex) {
        accountType = 'Basic';
      }
    }
    console.log('updateUserPlan plan', plan);

    var params = {
      TableName: userProfilesTable,
      Key: {
        cognitoId: userIdentityId
      },
      UpdateExpression: 'SET #account = :account, #plan = :plan',
      ExpressionAttributeValues: {
        ":account": {
          "type": accountType,
          "stripeId": stripeId
        },
        ":plan": {
          "id": plan.id,
          "name": plan.name,
          "amount": plan.amount,
          "interval": plan.interval,
          "trialPeriodDays": plan.trial_period_days
        }
      },
      ExpressionAttributeNames: {
        "#account": "account",
        "#plan": "plan"
      }
    };

    docClient.update(params, (err, data) => {
      if (err) {
        return reject(err);
      }
      resolve(data);
    });
  });
};

/**
 * Retrieves a user based on their stripe customer id
 */
module.exports.getCognitoIdFromStripeId = (customerId) => {
  return new Promise((resolve, reject) => {

    var params = {
      TableName: userProfilesTable,
      Key: {
        stripeId: customerId
      },
      AttributesToGet: [
        "cognitoId"
      ]
    };

    docClient.get(params, function(err, data) {
      if (err) {
        reject(err);
      }
      if (data === null) {
        reject("no stripeId associated with the user.");
      } // review and make standard
      resolve(data.item);
    });
  });
};

module.exports.getDeliverySchedulesForHour = () => {
  return new Promise((resolve, reject) => {
    var current = utils.UTCDate();
    var cutoff = utils.UTCDate();
    cutoff.setHours(current.getHours() - 23);

    var params = {
      TableName: emailSchedulesTable,
      IndexName: "hourOfDay",
      KeyConditionExpression: "hourOfDay = :hourOfDay",
      ExpressionAttributeValues: {
        ":hourOfDay": current.getUTCHours(),
        ":dayOfWeek": current.getUTCDay(),
        ":cutoff": cutoff.getTime()
      },
      FilterExpression: "contains(daysOfWeek, :dayOfWeek) AND (attribute_not_exists(lastEmailMs) OR lastEmailMs < :cutoff)"
    };

    console.log(JSON.stringify(params));
    docClient.query(params, (err, data) => {
      if (err) {
        return reject(err);
      }
      else {
        console.log(JSON.stringify(data));
        return resolve(data);
      }
    });
  });
};

module.exports.updateLastEmailDate = (userIdentityId) => {
  return new Promise((resolve, reject) => {
    var now = utils.UTCDate();
    var ticks = now.getTime();

    var params = {
      TableName: emailSchedulesTable,
      Key: {
        cognitoId: userIdentityId
      },
      UpdateExpression: 'SET lastEmailMs = :ticks',
      ExpressionAttributeValues: {
        ":ticks": ticks
      }
    };

    docClient.update(params, (err, data) => {
      if (err) {
        return reject(err);
      }
      resolve(data);
    });
  });
};