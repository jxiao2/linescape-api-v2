import Validator from "./validator.js";

var EmailScheduleValidator = new Validator({
  expectedProperties: [{
    key: "type",
    required: true,
    validator: function(value) {
      var errors = [];
      if (typeof value !== 'string') {
        return ["type must be a string ('portPair', 'vessel', or 'port')"];
      }

      if (['portPair', 'vessel', 'port'].indexOf(value) === -1) {
        return ["Invalid value for type. Valid values are 'portPair', 'vessel', or 'port'."];
      }

      return errors;
    }
  }, {
    key: "portId",
    required: false,
    validator: function(value, data) {
      var errors = [];
      var isPort = data.type === "port";

      if (isPort && typeof value === 'undefined') {
        return ["You must specify a portId for type 'port'."];
      }
      
      if (isPort && typeof value !== 'number') {
        return ["portId must be an integer."];
      }
      return errors;
    }
  }, {
    key: "originPortId",
    required: false,
    validator: function(value, data) {
      var errors = [];
      var isPortPair = data.type === "portPair";

      if (isPortPair && typeof value === 'undefined') {
        return ["You must specify an originPortId for type 'portPair'."];
      }
      
      if (isPortPair && typeof value !== 'number') {
        return ["originPortId must be an integer."];
      }
      return errors;
    }
  }, {
    key: "destinationPortId",
    required: false,
    validator: function(value, data) {
      var errors = [];
      var isPortPair = data.type === "portPair";

      if (isPortPair && typeof value === 'undefined') {
        return ["You must specify a destinationPortId for type 'portPair'."];
      }
      
      if (isPortPair && typeof value !== 'number') {
        return ["destinationPortId must be an integer."];
      }
      return errors;
    },
  }, {
    key: "vesselId",
    required: false,
    validator: function(value, data) {
      var errors = [];
      var isVessel = data.type === "vessel";
      if (isVessel && typeof value === 'undefined') {
        return ["You must specify a vesselId for type 'vessel'."];
      }
      
      if (isVessel && typeof value !== 'number') {
        return ["vesselId must be an integer."];
      }
      return errors;
    }
  }]
});

module.exports = EmailScheduleValidator;