"use strict";

import _ from 'lodash';

function Validator(opts) {
  var defaults = {
    expectedProperties: []
  };

  var options = _.extend(defaults, opts);
  this.options = options;
}

Validator.prototype.validate = function(obj) {
  var validationErrors = [];

  try {

    if (typeof obj === 'undefined' || !obj) {
      return ["Please specify body data."];
    }

    for (var i = 0; i < this.options.expectedProperties.length; ++i) {
      var customValidator = this.options.expectedProperties[i].validator;
      var propertyKey = this.options.expectedProperties[i].key;
      var required = this.options.expectedProperties[i].required || false;

      var property = obj[propertyKey];

      if (required && typeof property === 'undefined') {
        validationErrors.push(`${propertyKey} is required.`);
      }

      if (typeof customValidator === 'function') {
        validationErrors = _.concat(validationErrors, customValidator(property, obj));
      }
    }
  }
  catch (ex) {
    console.log(ex);
    validationErrors.push(ex);
  }

  return validationErrors;
};

module.exports = Validator;