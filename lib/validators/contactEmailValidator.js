import Validator from "./validator.js";
import utils from "../utils";

var ContactEmailValidator = new Validator({
  expectedProperties: [{
    key: "from",
    required: true,
    validator: function(value) {
      var errors = [];
      if (typeof value !== 'string') {
        return ["from must be a string"];
      }

      if (value.length > 1024) {
        return ["string must be less than 1024 characters"];
      }

      return errors;
    }
  }, {
    key: "email",
    required: true,
    validator: function(value, data) {
      var errors = [];

      if (!utils.isValidEmail(value)) {
        return [`'${value}' is not a valid email.`];
      }

      if (value.length > 1024) {
        return ["string must be less than 1024 characters"];
      }

      return errors;
    }
  }, 
  // ****
  {
    key: "domain", 
    required: true,
    validator: function(value, data) {
      var errors = [];
      if (value.length > 1024) {
        return ["string must be less than 1024 characters"];
      }

      return errors;
    }
  },
  // ****
  {
    key: "company",
    required: true,
    validator: function(value, data) {
      var errors = [];
      if (typeof value !== 'string') {
        return ["company must be a string"];
      }

      if (value.length > 1024) {
        return ["string must be less than 1024 characters"];
      }

      return errors;
    }
  }, {
    key: "position",
    required: true,
    validator: function(value, data) {
      var errors = [];
      if (typeof value !== 'string') {
        return ["company must be a string"];
      }

      if (value.length > 1024) {
        return ["string must be less than 1024 characters"];
      }

      return errors;
    }
  }, {
    key: "telephone",
    required: true,
    validator: function(value, data) {
      var errors = [];
      if (typeof value !== 'string') {
        return ["company must be a string"];
      }

      if (value.length > 1024) {
        return ["string must be less than 1024 characters"];
      }

      return errors;
    }
  }, {
    key: "country",
    required: true,
    validator: function(value, data) {
      var errors = [];
      if (typeof value !== 'string') {
        return ["company must be a string"];
      }

      if (value.length > 1024) {
        return ["string must be less than 1024 characters"];
      }

      return errors;
    }
  }, {
    key: "message",
    required: true,
    validator: function(value, data) {
      var errors = [];
      if (typeof value !== 'string') {
        return ["company must be a string"];
      }

      if (value.length > 1024) {
        return ["string must be less than 1024 characters"];
      }

      return errors;
    }
  }]
});

module.exports = ContactEmailValidator;