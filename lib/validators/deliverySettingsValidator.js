import Validator from "./validator.js";
import momentTimezone from "moment-timezone";
import utils from "../utils";

var DeliverySettingsValidator = new Validator({
  expectedProperties: [{
    key: "daysOfWeek",
    required: true,
    validator: function(value) {
      var errors = [];
      if (!Array.isArray(value)) {
        return ["daysOfWeek must be an array of integers."];
      }

      if (value.length > 0 && typeof value[0] !== 'number') {
        return ["daysOfWeek must be an array of integers."];
      }

      for (var i = 0; i < value.length; ++i) {
        var val = value[i];

        if (typeof val !== 'number') {
          errors.push(`${value[i]} is invalid. Must be an integer between 1 and 5`);
        }
        else if (val < 1 || val > 5) {
          errors.push(`${val} is invalid for daysOfWeek. Must be between 1 and 5.`);
        }
      }
      return errors;
    }
  }, {
    key: "hourOfDay",
    required: true,
    validator: function(value) {
      var errors = [];
      if (typeof value !== 'number') {
        return ["hourOfDay must be an integer between 0 and 23."];
      }
      if (value < 0 || value > 23) {
        return ["hourOfDay must be between 0 and 23."];
      }
      return errors;
    }
  }, {
    key: "htmlEmails",
    required: true,
    validator: function(value) {
      if (typeof value !== 'boolean') {
        return ["htmlEmails must be a boolean"];
      }
      return [];
    }
  }, {
    key: "emailFormat",
    required: true,
    validator: function(value) {
      if (typeof value !== 'string') {
        return ["emailFormat must be a string ('csv' or 'html')"];
      }
      var lowered = value.toLowerCase();

      if (['csv', 'html'].indexOf(value) === -1) {
        return [`'${value} is not valid for emailFormat. Must be 'csv' or 'html'`];
      }

      return [];
    }
  }, {
    key: "otherEmails",
    required: false,
    validator: function(value) {
      var errors = [];
      if (typeof value === 'undefined') {
        return errors;
      }
      if (!Array.isArray(value)) {
        return ["otherEmails must be an array of strings."];
      }

      if (value.length > 5) {
        return ["Only 5 additional delivery emails may be specified."];
      }
      for (var i = 0; i < value.length; ++i) {
        var val = value[i];

        if (typeof val !== 'string') {
          errors.push(`'${value[i]}' is invalid. Must be an email string.`);
        }
        else if (!utils.isValidEmail(val)) {
          errors.push(`'${val}' is not a valid email.`);
        }
      }

      return errors;
    },
  }, {
    key: "emailSelf",
    required: true,
    validator: function(value) {
      if (typeof value !== 'boolean') {
        return ["emailSelf must be a boolean"];
      }
      return [];
    }
  }, {
    key: "userTimeZone",
    required: true,
    validator: function(value) {
      if (typeof value !== 'string') {
        return ["userTimeZone must be a string."];
      }
      var validTimeZones = momentTimezone.tz.names();
      var userTimeZone = value.replace(/ /g, '_');
      if (validTimeZones.indexOf(userTimeZone) === -1) {
        return [`'${value}' is not a valid timezone. Please call GET /timezones to get a list of valid timezones.`];
      }
      return [];
    }
  }]
});

module.exports = DeliverySettingsValidator;