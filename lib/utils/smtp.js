"use strict";

import nodemailer from "nodemailer";
import fs from "fs";
import _ from "lodash";
  
let transporter = nodemailer.createTransport({
  host: "hyperion.tecture.com",
  port: 587,
  secure: false, // use SSL
  auth: {
    user: 'linescape@hermes.tecture.com',
    pass: 'vBPeqTAgXw4hJv4'
  }
});

module.exports.send = (fromAddress, toAddresses, subject, text, html, attachments) => {
  if (attachments == null) {
    attachments = [];
  }
  
  // push the logo image to the attachments
  attachments.push({
      filename: 'linescape-logo-gradient.png',
      path: process.env.SERVERLESS_STAGE === 'beta' ? 'http://linescape-dev.tecture.net/img/linescape-logo-gradient.png' : 'https://www.linescape.com/img/linescape-logo-gradient.png',
      cid: 'linescape-loge-gradient-image'
  });


  // setup e-mail data with unicode symbols
  var mailOptions = {
    from: fromAddress, //'"Fred Foo 👥" <foo@blurdybloop.com>', // sender address
    to: toAddresses, // list of receivers
    subject: subject, // Subject line
    text: text, // plaintext body
    html: html, // html body,
    attachments: attachments
  };

  if (fromAddress === "data@linescape.com") {
    fromAddress = '"Linescape Data" <data@linescape.com>';
    mailOptions["bcc"] = "databyemail@linescape.com, linescape@tecture.com";
  }
   else if (toAddresses === "contacts@linescape.com") {
    mailOptions["bcc"] = "linescape@pipedrivemail.com, linescape@tecture.com";
   }
  else if (fromAddress === "contacts@linescape.com") {
    mailOptions["bcc"] = "linescape@pipedrivemail.com, linescape@tecture.com";
  }
  
  // send mail with defined transport object
  transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
      return console.log(error);
    }
    console.log('Message sent: ' + info.response);
  });
};


/**
 * Compiles the schedule HTML email template and returns it as a string.
 */
module.exports.createHTMLResultsForEmail = dataObj => {
  var type = getEmailTypeFromObj(dataObj);
  dataObj.website = process.env.SERVERLESS_STAGE === 'beta' ? 'http://linescape-dev.tecture.net' : 'https://www.linescape.com';

  let template = fs.readFileSync(`lib/templates/${type}.html`, 'utf8');
  var compiled = _.template(template);
  var result = compiled(dataObj);
  
  return result;
};

var getEmailTypeFromObj = (dataObj) => {
  function isNotNil(obj) {
    return !_.isNil(obj);
  }
  if (isNotNil(dataObj.schedules)) {
    return "schedule";
  }
  if (isNotNil(dataObj.ports)) {
    return "port";
  }
  if (isNotNil(dataObj.vessels)) {
    return "vessel";
  }
  if (isNotNil(dataObj.carriers)) {
    return "carrier";
  }
  if (isNotNil(dataObj.locations)) {
    return "location";
  }
  if (isNotNil(dataObj.contact)) {
    return "contact";
  }
  if (isNotNil(dataObj.share)) {
    return "share";
  }
};