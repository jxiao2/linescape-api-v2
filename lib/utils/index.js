'use strict';

import Promise from 'bluebird';
import https from 'https';
import _ from 'lodash';
import accountTypes from "../globals/accountTypes.js";
import DISALLOWED_KEYS from "../globals/disallowedKeys.js";
import stringify from "csv-stringify";
import moment from "moment";

module.exports.createCsv = (content, columns) => {
  return new Promise(function (resolve, reject) {
    try {
      stringify(content, {
        header: true
      }, function (err, data) {
        if (err) {
          console.log("error in csv", err);
          return reject(err);
        }
        return resolve(data);
      });
    }
    catch (ex) {
      console.log("err converting csv", ex);
    }
  });
};

/**
 * Converts API resources into a readable format for use
 * in HTML emails and CSVs.
 */
module.exports.mapCsv = (items, type) => {
  // Internal functions
  var atItem = (item, path) => {
    return _.first(_.at(item, [path])) || "";
  };
  console.log('utils mapCsv items', items, 'type', type);
  var mapSchedule = (items) => {
    var result = [];

    for (var i = 0; i < items.length; ++i) {
      var atItemLocal = atItemCurried(items[i]);
      result.push({
        carrierId: atItemLocal("carrier.id"),
        carrierSCAC: atItemLocal("carrier.scac"),
        carrierName: atItemLocal("carrier.name"),
        routeCode: atItemLocal("route.code"),
        routeName: atItemLocal("route.name"),
        vesselIMO: atItemLocal("vessel.imo"),
        vesselName: atItemLocal("vessel.name"),
        voyage: atItemLocal("voyage"),
        portCode: atItemLocal("end_port.code"),
        portName: atItemLocal("end_port.name"),
        etdDate: atItemLocal("start_date"),
        etaDate: atItemLocal("end_date")
      });
    }

    return result.sort(function (a, b) {
      var asd = a.startDate;
      var bsd = b.startDate;

      if (asd < bsd) { return -1; }
      if (asd > bsd) { return 1; }

      var aed = a.endDate;
      var bed = b.endDate;

      if (aed < bed) { return -1; }
      if (aed > bed) { return 1; }

      return 0;
    });
  };

  var mapCall = (items) => {
    var result = [];

    for (var i = 0; i < items.length; ++i) {
      var atItemLocal = atItemCurried(items[i]);
      result.push({
        carrierId: atItemLocal("carrier.id"),
        carrierSCAC: atItemLocal("carrier.scac"),
        carrierName: atItemLocal("carrier.name"),
        routeCode: atItemLocal("route.code"),
        routeName: atItemLocal("route.name"),
        vesselIMO: atItemLocal("vessel.imo"),
        vesselName: atItemLocal("vessel.name"),
        voyage: atItemLocal("voyage"),
        portCode: atItemLocal("port.code"),
        portName: atItemLocal("port.name"),
        etaDate: atItemLocal("eta"),
        etdDate: atItemLocal("etd")
      });
    }

    return result.sort(function (a, b) {
      var ac = a.carrierSCAC.toLowerCase();
      var bc = b.carrierSCAC.toLowerCase();

      if (ac < bc) { return -1; }
      if (ac > bc) { return 1; }

      var ae = a.etaDate;
      var be = b.etaDate;

      if (ae < be) { return -1; }
      if (ae > be) { return 1; }

      return 0;
    });
  };

  var mapTrip = (items) => {
    var result = [];

    for (var i = 0; i < items.length; ++i) {
      var atItemLocal = atItemCurried(items[i]);

      var start = atItemCurried(items[i].legs[0].start);
      var end = atItemCurried(items[i].legs[items[i].legs.length - 1].end);

      var legs = items[i].legs.length;
      items[i].transhipments = 'direct';

      if (legs > 1) {
        items[i].transhipments = (legs - 1) + ' t/s';
      }

      var legOne = atItemCurried(items[i].legs[0]);
      var legTwo = atItemCurried(items[i].legs[1]);
      var legThree = atItemCurried(items[i].legs[2]);

      result.push({
        "carrierId": atItemLocal("carrier.id"),
        "carrierSCAC": atItemLocal("carrier.scac"),
        "carrierName": atItemLocal("carrier.name"),
        transhipments: atItemLocal("transhipments"),
        "routeCode-1": legOne("route.code"),
        "routeName-1": legOne("route.name"),
        "vesselIMO-1": legOne("vessel.imo"),
        "vesselName-1": legOne("vessel.name"),
        "voyage-1": legOne("voyage"),
        startPortCode: start("port.code"),
        startPortName: start("port.name"),
        startDate: start("date"),
        "ts1PortCode": legTwo("start.port.code"),
        "ts1PortName": legTwo("start.port.name"),
        "ts1etaDate": legTwo("start.port.code") ? legOne("end.date") : null,
        "routeCode-2": legTwo("route.code"),
        "routeName-2": legTwo("route.name"),
        "vesselIMO-2": legTwo("vessel.imo"),
        "vesselName-2": legTwo("vessel.name"),
        "voyage-2": legTwo("voyage"),
        "ts1etdDate": legTwo("start.date"),
        "ts2PortCode": legThree("start.port.code"),
        "ts2PortName": legThree("start.port.name"),
        "ts2etaDate": legThree("start.port.code") ? legTwo("end.date") : null,
        "routeCode-3": legThree("route.code"),
        "routeName-3": legThree("route.name"),
        "vesselIMO-3": legThree("vessel.imo"),
        "vesselName-3": legThree("vessel.name"),
        "voyage-3": legThree("voyage"),
        "ts2etdDate": legThree("start.date"),
        endPortCode: end("port.code"),
        endPortName: end("port.name"),
        endDate: end("date"),
        duration: atItemLocal("duration")
      });
    }

    return result.sort(function (a, b) {
      var asd = a.startDate;
      var bsd = b.startDate;

      if (asd < bsd) { return -1; }
      if (asd > bsd) { return 1; }

      var aed = a.endDate;
      var bed = b.endDate;

      if (aed < bed) { return -1; }
      if (aed > bed) { return 1; }

      return 0;
    });
  };

  var mapPort = (items) => {
    var result = [];

    for (var i = 0; i < items.length; ++i) {
      var atItemLocal = atItemCurried(items[i]);
      result.push({
        id: atItemLocal("id"),
        name: atItemLocal("name"),
        code: atItemLocal("code"),
        countryName: atItemLocal("country.name"),
        countryId: atItemLocal("country.id"),
        regionId: atItemLocal("region.id"),
        regionName: atItemLocal("region.name"),
        latitute: atItemLocal("latlong[0]"),
        longitude: atItemLocal("latlong[1]")
      });
    }

    return result;
  };

  var mapCarrier = (items) => {
    var result = [];

    for (var i = 0; i < items.length; ++i) {
      var atItemLocal = atItemCurried(items[i]);
      result.push({
        id: atItemLocal("id"),
        city: atItemLocal("city"),
        fax: atItemLocal("fax"),
        maxShips: atItemLocal("max_ships"),
        phone: atItemLocal("phone"),
        maxTEU: atItemLocal("max_teu"),
        email: atItemLocal("email"),
        postcode: atItemLocal("postcode"),
        regions: atItemLocal("regions"),
        address: atItemLocal("address"),
        website: atItemLocal("website"),
        description: atItemLocal("description"),
        established: atItemLocal("established"),
        countryId: atItemLocal("country.id"),
        countryName: atItemLocal("country.name"),
        scac: atItemLocal("scac"),
        carrierGroup: atItemLocal("carrier_group"),
        company: atItemLocal("company"),
        yearEstablished: atItemLocal("year_est"),
        name: atItemLocal("name")
      });
    }
    return result;
  };


  var mapVessel = (items) => {
    var result = [];

    for (var i = 0; i < items.length; ++i) {
      var atItemLocal = atItemCurried(items[i]);
      result.push({
        id: atItemLocal("id"),
        portRegistry: atItemLocal("port_registry"),
        netTonnage: atItemLocal("net_tonnage"),
        avgTEU: atItemLocal("avg"),
        reeferTEU: atItemLocal("reefer_teu"),
        ownerId: atItemLocal("owner.id"),
        ownerName: atItemLocal("owner.name"),
        speed: atItemLocal("speed"),
        callSign: atItemLocal("call_sign"),
        shipBuilder: atItemLocal("ship_builder"),
        co2: atItemLocal("co2"),
        dwt: atItemLocal("dwt"),
        engineDesign: atItemLocal("engine_design"),
        maxTEU: atItemLocal("max_teu"),
        imo: atItemLocal("imo"),
        previousNames: atItemLocal("previous_names"),
        yardHull: atItemLocal("yard_hull"),
        engineType: atItemLocal("engine_type"),
        power: atItemLocal("poer"),
        draught: atItemLocal("draught"),
        mmsi: atItemLocal("mmsi"),
        beam: atItemLocal("beam"),
        shipType: atItemLocal("ship_type"),
        sfc: atItemLocal("sfc"),
        yearBuilt: atItemLocal("year_built"),
        name: atItemLocal("name"),
        countryId: atItemLocal("country.id"),
        countryName: atItemLocal("country.name"),
        grossTonnage: atItemLocal("gross_tonnage"),
        length: atItemLocal("length"),
        handlingGear: atItemLocal("handling_gear"),
        carrierName: atItemLocal("carrier.name"),
        carrierSCAC: atItemLocal("carrier.scac"),
        carrierId: atItemLocal("carrier.id")
      });
    }

    return result;
  };

  var mapLocation = (items) => {
    var result = [];

    for (var i = 0; i < items.length; ++i) {
      var atItemLocal = atItemCurried(items[i]);
      var item = {
        id: atItemLocal("id"),
        type: atItemLocal("type"),
        name: atItemLocal("value.name")
      };

      result.push(item);
    }

    return result;
  };

  // Mapping function selection
  if (_.isNil(type)) {
    type = "schedule";
  }

  var atItemCurried = _.curry(atItem);
  var mappingFunction;

  switch (type.toLowerCase()) {
    case "carrier":
      mappingFunction = mapCarrier;
      break;
    case "port":
      mappingFunction = mapPort;
      break;
    case "vessel":
      mappingFunction = mapVessel;
      break;
    case "location":
      mappingFunction = mapLocation;
      break;
    case "trip":
      mappingFunction = mapTrip;
      break;
    case "schedule":
      mappingFunction = mapSchedule;
      break;
    case "carrierschedule":
    case "portschedule":
      mappingFunction = mapCall;
      break;
    case "vesselschedule":
      mappingFunction = mapCall;
      break;
  }

  return mappingFunction(items);
};


module.exports.PromiseRequest = Promise.method((options) => {
  return new Promise(function (resolve, reject) {
    var request = https.request(options, function (response) {
      // Bundle the result
      var result = {
        'httpVersion': response.httpVersion,
        'httpStatusCode': response.statusCode,
        'headers': response.headers,
        'body': '',
        'trailers': response.trailers,
      };

      // Build the body
      response.on('data', function (chunk) {
        result.body += chunk;
      });

      response.on('end', function () {
        resolve(result);
      });
      // Resolve the promise
    });

    // Handle errors
    request.on('error', function (error) {
      console.log('Problem with request:', error.message);
      reject(error);
    });

    // Must always call .end() even if there is no data being written to the request body
    request.end();
  });
});

/**
 * Adds information about the item related to the user.
 * @param {bool} favorited - Indicates whether the user has favorited the item.
 * @param {string} note - The note the user has associated with the item.
 */
module.exports.getUserData = (favorited, note) => {
  return {
    favorited: favorited,
    note: note || ''
  };
};

/**
 * Allows testing without IAM authentication. Makes it easier to
 * call the endpoints.
 */
module.exports.getCognitoId = (event) => {
  if (process.env.SERVERLESS_STAGE === 'dev' &&
    process.env.MOCK_COGNITO_ID === "true") {
    return "us-east-1:8a274996-b3b5-4b5a-9c9c-f9c432e447ec";
  }
  return event.cognitoIdentityId;
};

module.exports.getParam = (params, key, variableType) => {
  var parsed = JSON.parse(params);
  switch (variableType) {
    case 'string':
      return parsed[key];
    case 'int':
      return parseInt(parsed[key]);
    default:
      return parsed[key];
  }
};

module.exports.isValidEmail = (email) => {
  return email.indexOf("@") !== -1;
};

module.exports.getSearchObjectFromQuery = (query, searchType, additionalParams) => {
  var queryObj = {};
  switch (searchType.toLowerCase()) {
    case "trip":
      queryObj = {
        limit: query.limit || 20,
        offset: query.offset || 0,
        origin_search: query.origin_search || "Origin",
        origin_ids: query.origin_ids || null,
        destination_search: query.destination_search || "Destination",
        destination_ids: query.destination_ids || null,
        "duration.lte": query["duration.lte"] || null,
        "duration.gte": query["duration.gte"] || null,
        "start_date.gte": query["start_date.gte"] || null,
        "start_date.lte": query["start_date.lte"] || null,
        "start_date.lt": query["start_date.lt"] || null,
        "end_date.gte": query["end_date.gte"] || null,
        "end_date.lte": query["end_date.lte"] || null,
        "end_date.lt": query["end_date.lt"] || null,
        "carriers": query.carriers || null,
        "carriers.ne": query["carriers.ne"] || null,
        "transhipment_counts": query.transhipment_counts || null,
        "sort": query.sort || null,
        "with_nearby_origins": query.with_nearby_origins || 0,
        "with_nearby_destinations": query.with_nearby_destinations || 0
      };
      break;
    case "schedule":
      queryObj = {
        limit: query.limit || 20,
        offset: query.offset || 0,
        id: query.id || null,
        vessel_id: query.vessel_id || null,
        carrier_id: query.carrier_id || null,
        start_port_id: query.start_port_id || null,
        end_port_id: query.end_port_id || null,
        "start_date.gte": query["start_date.gte"] || null,
        "start_date.lte": query["start_date.lte"] || null,
        "start_date.lt": query["start_date.lt"] || null,
        "end_date.gte": query["end_date.gte"] || null,
        "end_date.lte": query["end_date.lte"] || null,
        "end_date.lt": query["end_date.lt"] || null
      };
      break;
    case "portschedule":
      queryObj = {
        limit: query.limit || 20,
        search: query.search || null,
        offset: query.offset || 0,
        schedule_id: query.schedule_id || null,
        carriers: query.carriers || null,
        "carriers.ne": query["carriers.ne"] || null,
        "eta.gte": query["eta.gte"] || null,
        "eta.lte": query["eta.lte"] || null,
        "eta.lt": query["eta.lt"] || null,
        "etd.gte": query["etd.gte"] || null,
        "etd.lte": query["etd.lte"] || null,
        "etd.lt": query["etd.lt"] || null
      };
      break;
    case "port":
      queryObj = {
        limit: query.limit || 20,
        search: query.search || null,
        offset: query.offset || 0,
        name: query.name || null,
        id: query.id || null,
        country_id: query.country_id || null,
        region_id: query.region_id || null,
        major: true,
        functype: 'port'
      };
      break;
    case "vesselschedule":
      // same as portschedule
      // added for consistency?
      queryObj = {
        limit: query.limit || 20,
        search: query.search || null,
        offset: query.offset || 0,
        schedule_id: query.schedule_id || null,
        carriers: query.carriers || null,
        "eta.gte": query["eta.gte"] || null,
        "eta.lte": query["eta.lte"] || null,
        "eta.lt": query["eta.lt"] || null,
        "etd.gte": query["etd.gte"] || null,
        "etd.lte": query["etd.lte"] || null,
        "etd.lt": query["etd.lt"] || null,
        "sort": "carrier,schedule,schedule_index,eta"
      };
      break;
    case "vessel":
      queryObj = {
        limit: query.limit || 20,
        offset: query.offset || 0,
        id: query.id || null,
        call_sign: query.call_sign || null,
        mmsi: query.mmsi || null,
        name: query.name || null,
        search: query.search || null,
        carrier_id: query.carrier_id || null,
        "sort": "vessel_name"
      };
      break;
    case "carrier":
      queryObj = {
        limit: query.limit || 20,
        search: query.search || null,
        offset: query.offset || 0,
        name: query.name || null,
        id: query.id || null,
        scac: query.scac || null
      };
      break;
    case "localcontact":
      queryObj = {
        limit: query.limit || 20,
        offset: query.offset || 0,
        country_id: query.country_id || null
      };
      break;
    case "location":
      queryObj = {
        limit: query.limit || 20,
        search: query.search || null,
        offset: query.offset || 0,
        id: query.id || null,
        major: true
      };
      break;
    default:
      console.log(`searchType '${searchType}' was not recognized`);
      break;
  }
  if (typeof additionalParams !== 'undefined') {
    queryObj = _.extend(queryObj, additionalParams);
  }

  console.log("in utils", JSON.stringify(queryObj));
  return queryObj;
};

module.exports.UTCDate = () => {
  return new Date(new Date().toUTCString());
};

module.exports.obfuscateResources = (accountTypeId, resources, resourceType) => {
  var obfuscatedResources = [];
  if (accountTypeId === accountTypes.professional) {
    return resources;
  }

  for (var i = 0; i < resources.length; ++i) {
    resources[i] = module.exports.obfuscateResource(accountTypeId, resources[i], resourceType);
  }
  return resources;
};

module.exports.obfuscateResource = (accountTypeId, resource, resourceType) => {
  if (resourceType === 'date') {
    return module.exports.obfuscateDate(accountTypeId, resource);
  } else if (resourceType == 'time') {
    return module.exports.obfuscateTime(accountTypeId, resource);
  }

  if (accountTypeId === accountTypes.professional) {
    return resource;
  }

  var disallowedKeys = DISALLOWED_KEYS[resourceType][accountTypeId];
  return _.omit(resource, disallowedKeys);
};

module.exports.obfuscateDate = (accountTypeId, resource) => {
  if (resource === null) {
    return ' - ';
  }

  return resource;

  // let today = moment();
  // let limit = today.clone().add(2, 'weeks').startOf('day'); // unregistered

  // if (accountTypeId === accountTypes.professional) {
  //   return resource;
  // }
  // else if (accountTypeId === accountTypes.premium) {
  //   limit = today.clone().add(3, 'weeks').startOf('day');
  // }
  // else if (accountTypeId === accountTypes.basic) {
  //   limit = today.clone().add(2, 'weeks').startOf('day');
  // }

  // let item = moment(resource);
  // return item.isAfter(limit) ? 'BLURRED TEXT' : item.format('YYYY-MM-DD').toString();
};

module.exports.obfuscateTime = (accountTypeId, resource) => {
  if (resource === null) {
    return '';
  }

  return resource;
}