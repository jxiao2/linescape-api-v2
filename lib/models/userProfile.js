"use strict";

function UserProfile(cognitoId) {
  this.cognitoId = cognitoId;
  this.favorites = {};
  this.limits = {
    searchCount: 0
  };
  this.plan = {};
  this.recentSearches = [];

  /*this.favorites = {
    ports: [],
    vessels: [],
    schedules: [],
    carriers: []
  };
  
  this.limits = {};
  this.recentSearches = [];
  
  this.plan = {};*/
}

module.exports = UserProfile;