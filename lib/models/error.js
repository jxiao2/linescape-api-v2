"use strict";

function Error(status, message, errorCode, details) {
  this.status = status;
  this.message = message || "An unknown error occurred.";
  this.errorCode = errorCode || "-1";
  this.details = details || null;
}

module.exports = Error;