'use strict';

import handlers from "../../handlers/jobs.js";

module.exports.handler = function(event, context, cb) {
  handlers.sendScheduleEmails(event, context, cb);
};