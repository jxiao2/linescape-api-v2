'use strict';

import querystring from 'querystring';
import utils from "../utils";

const hostName = 'api.linescape.com';

const portsEndpoint = '/ws/v1/ports';
const locationsEndpoint = '/ws/v1/locations';
const callsEndpoint = '/ws/v1/calls';
const vesselsEndpoint = '/ws/v1/vessels';
const tripsEndpoint = '/ws/v1/trips'; /* /schedules */
const carriersEndpoint = '/ws/v1/carriers';
const schedulesEndpoint = '/ws/v1/schedules'; /* /vessels/{vesselId}/schedules */
const localcontactsEndpoint = '/ws/v1/contacts';

const headers = {
  'x-api-key': process.env.DATA_API_KEY
};

module.exports.locations = (query) => {
  var options = getDefaultOptions(locationsEndpoint, query, "GET");

  return new utils.PromiseRequest(options).then((response) => {
    var json = JSON.parse(response.body);
    json.lshiftPath = options.path;
    return json;
  });
};

module.exports.ports = (query) => {
  var options = getDefaultOptions(portsEndpoint, query, "GET");

  return new utils.PromiseRequest(options).then((response) => {
    var json = JSON.parse(response.body);
    json.lshiftPath = options.path;
    return json;
  });
};

module.exports.calls = (query, portId) => {
  var options = getDefaultOptions(callsEndpoint, query, "GET");

  return new utils.PromiseRequest(options).then((response) => {
    var json = JSON.parse(response.body);
    json.lshiftPath = options.path;
    return json;
  });
};

module.exports.vessels = (query) => {
  var options = getDefaultOptions(vesselsEndpoint, query, "GET");

  return new utils.PromiseRequest(options).then((response) => {
    var json = JSON.parse(response.body);
    json.lshiftPath = options.path;
    return json;
  });
};

module.exports.trips = (query) => {
  var options = getDefaultOptions(tripsEndpoint, query, "GET");

  return new utils.PromiseRequest(options).then((response) => {
    var json = JSON.parse(response.body);
    json.lshiftPath = options.path;
    return json;
  });
};

module.exports.carriers = (query) => {
  var options = getDefaultOptions(carriersEndpoint, query, "GET");

  return new utils.PromiseRequest(options).then((response) => {
    var json = JSON.parse(response.body);
    json.lshiftPath = options.path;
    return json;
  });
};

module.exports.schedules = (query, id, type) => {
  var options = getDefaultOptions(schedulesEndpoint, query, "GET");

  return new utils.PromiseRequest(options).then((response) => {
    var json = JSON.parse(response.body);
    json.lshiftPath = options.path;
    return json;
  });
};

module.exports.localcontacts = (query, id) => {
  var options = getDefaultOptions(localcontactsEndpoint, query, "GET");

  return new utils.PromiseRequest(options).then((response) => {
    var json = JSON.parse(response.body);
    json.lshiftPath = options.path;
    return json;
  });
};

function getDefaultOptions(endpoint, queryParams, method) {
  return {
    hostname: hostName,
    port: 443,
    path: endpoint + '/?' + querystring.stringify(queryParams || {}),
    method: method || "GET",
    headers: headers
  };
}