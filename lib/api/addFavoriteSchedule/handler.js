'use strict';

import Error from "../../models/error.js";
import handlers from "../../handlers/favorites.js";

module.exports.handler = function(event, context, callback) {
  handlers.addFavoriteSchedule(event, context, callback);
};
