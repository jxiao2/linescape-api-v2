'use strict';

import handlers from "../../handlers/favorites.js";

module.exports.handler = function(event, context, callback) {
  handlers.addFavoriteRoute(event, context, callback);
};