'use strict';

import handlers from "../../handlers/search.js";

module.exports.handler = function(event, context, callback) {
  handlers.shareSearch(event, context, callback);
};
