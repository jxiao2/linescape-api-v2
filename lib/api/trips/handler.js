'use strict';

import handlers from "../../handlers/dataApi.js";

module.exports.handler = function(event, context, callback) {
  handlers.trips(event, context, callback);
};
