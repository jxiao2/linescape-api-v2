"use strict";

import handlers from "../../handlers/dataApi.js";
import Error from "../../models/error.js";
import smtp from "../../utils/smtp.js";

module.exports.handler = function(event, context, callback) {
  handlers.ports(event, context, callback);
};