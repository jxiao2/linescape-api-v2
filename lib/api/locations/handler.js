'use strict';

import handlers from "../../handlers/dataApi.js";

module.exports.handler = function(event, context, cb) {
  handlers.locations(event, context, cb);
};
