"use strict";

import handlers from "../../handlers/user.js";
import Error from "../../models/error.js";

module.exports.handler = function(event, context, callback) {
  handlers.updateDeliverySettings(event, context, callback);
};