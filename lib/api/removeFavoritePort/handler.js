'use strict';

import handlers from "../../handlers/favorites.js";

module.exports.handler = function(event, context, cb) {
  handlers.removeFavoritePort(event, context, cb);
};
