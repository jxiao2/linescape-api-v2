"use strict";

import handlers from "../../../handlers/billing.js";

module.exports.handler = function(event, context, callback) {
  handlers.cancelSubscription(event, context, callback);
};