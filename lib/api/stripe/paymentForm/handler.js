"use strict";

import handlers from "../../handlers/billing.js";
import Error from "../../models/error.js";

module.exports.handler = function(event, context, callback) {
  handlers.paymentForm(event, context, callback);
};