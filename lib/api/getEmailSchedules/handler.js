'use strict';

import handlers from "../../handlers/user.js";

module.exports.handler = function(event, context, callback) {
  handlers.getEmailSchedules(event, context, callback);
};
