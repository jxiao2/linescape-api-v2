"use strict";

import handlers from "../../handlers/user.js";

module.exports.handler = function(event, context, callback) {
  handlers.getDeliverySettings(event, context, callback);
};