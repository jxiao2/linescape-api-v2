'use strict';

import handlers from "../../handlers/dataApi.js";

module.exports.handler = function(event, context, callback) {
  handlers.getSchedulesByVessel(event, context, callback);
};
