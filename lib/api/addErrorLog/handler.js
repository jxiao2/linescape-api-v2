'use strict';

import handlers from "../../handlers";

module.exports.handler = function(event, context, cb) {
  handlers.addErrorLog(event, context, cb);
};