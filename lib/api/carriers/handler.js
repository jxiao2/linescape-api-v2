'use strict';

import handlers from "../../handlers/dataApi.js";

module.exports.handler = function(event, context, callback) {
  handlers.carriers(event, context, callback);
};
