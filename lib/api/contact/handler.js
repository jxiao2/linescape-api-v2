'use strict';

import handlers from "../../handlers/user.js";

module.exports.handler = function(event, context, cb) {
  handlers.contact(event, context, cb);
};
