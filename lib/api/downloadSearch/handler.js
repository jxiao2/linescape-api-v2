'use strict';

import handlers from "../../handlers/search.js";

module.exports.handler = function(event, context, cb) {
  handlers.downloadSearch(event, context, cb);
};
