'use strict';

import handlers from "../../handlers";

module.exports.handler = function(event, context, callback) {
  handlers.timezones(event, context, callback);
};
