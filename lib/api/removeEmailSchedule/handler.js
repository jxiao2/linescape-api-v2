'use strict';

import handlers from "../../handlers/user.js";

module.exports.handler = function(event, context, callback) {
  try {
    handlers.removeEmailSchedule(event, context, callback);
  } catch (ex) {
    console.log(ex);
  }
};
